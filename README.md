iFoldRNA

# Set iFoldRNA environment variables

    export IFOLDRNA_HOME=/home/xxx/programs/HRP-2.0
    export PATH=$IFOLDRNA_HOME/bin:$PATH

# Initialize the working directory

    mkdir test
    cd test
    init-ifoldrna .

# Prepare the sequence file and the secondary structure file

sequence file example:

    AAAACCCCUUUU

secondary structure file example:

    ((((....))))

# Prepare the restraints file [Optional]

restraints file example:

    A/1/C4' A/12/C4' 0 20
    A/2/C4' A/11/C4' 0 20
    A/3/C4' A/10/C4' 0 20
    A/4/C4' A/9/C4' 0 20

# Prepare the configuration file

The "init-ifoldrna" command has already prepared a "config.txt" file, so users only need to modify the "config.txt" file.

# iFoldRNA Simulation

Example:

    for i in $(seq 1 8); do             
        perl -lane 's/s\s+(\d+)/s '${i}'/g;s/out test/out test-'${i}'/g;print' config.txt >config-${i}.txt
        nohup ifoldrna config-${i}.txt &                                                                                                      
    done 

Here we submitted 8 simulations with different random numbers.

# Calculate RMSD

1. Prepare the native pdb file.

2. Convert the native pdb file to 'ifold' format.

        pdb2ifold native.pdb native.ifold

3. Calculate the RMSD and also retrieve the energies from the simulation.

        cat *.ifold >all.ifold
        cat native.ifold all.ifold >native-all.ifold
        ifoldrna-rmsd native-all.ifold native-all.rmsd_e

# Clustering

Example:

    ifoldrna-cluster all.ifold 0.01 15

Here, 0.01 is the percentage of lowest energy structures for clustering. 15 is the RMSD cutoff.

# Calculate the number of poses in an ifold file.

    perl -lane '$n++;END{print $n/2.0}' all.ifold

# Extract a specific pose from an ifold file.

Get the 1st pose:

    perl -lane 'BEGIN{$n=-1};$n++;print if int($n/2)==-1+'1 all.ifold >pose-1.ifold

Get the 5th pose:

    perl -lane 'BEGIN{$n=-1};$n++;print if int($n/2)==-1+'5 all.ifold >pose-5.ifold

Get the 100th pose:

    perl -lane 'BEGIN{$n=-1};$n++;print if int($n/2)==-1+'100 all.ifold >pose-100.ifold

# Conver ifold file to PDB files.

    ifold2pdb native.pdb all-clust01.ifold

If there is no native pdb file, it can be replaced by a reference file.

    ifoldrna-ref seq.txt ss.txt ref.pdb
    ifold2pdb ref.pdb all-clust01.ifold

