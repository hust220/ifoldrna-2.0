import numpy as np
import sys
import re

in_file = sys.argv[1]
percentile = float(sys.argv[2])

ls = []

for line in open(in_file):
    l = re.split('\s+', line.strip())
    a, b, v = int(l[0]), int(l[1]), float(l[2])
    ls.append([a, b, v])

values = [p[2] for p in ls]

cutoff = np.percentile(values, percentile)

for p in ls:
    if p[2] > cutoff:
        print('{0} {1} {2:.10f}'.format(p[0], p[1], p[2]))
