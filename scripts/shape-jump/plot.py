import numpy as np
import matplotlib.pyplot as plt
import sys

data_file = sys.argv[1]
outname = sys.argv[2]

data = np.genfromtxt(data_file)

x = data.T[0]
y = data.T[1]

xmin = min(x)
xmax = max(x)

plt.xlim(xmin, xmax)
plt.scatter(x, y)
plt.xlabel('Deletion Rate')
plt.ylabel('Distance')

#plt.show()
plt.savefig(outname)
