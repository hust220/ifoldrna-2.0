import numpy as np
import sys
import re

in_file = sys.argv[1]
percentile1 = float(sys.argv[2])
percentile2 = float(sys.argv[3])

ls = []

for line in open(in_file):
    l = re.split('\s+', line.strip())
    a, b, v = int(l[0]), int(l[1]), float(l[2])
    ls.append([a, b, v])

values = [p[2] for p in ls]

cutoff1 = np.percentile(values, percentile1)
cutoff2 = np.percentile(values, percentile2)

for p in ls:
    if p[2] >= cutoff1 and p[2] < cutoff2:
        print('{0} {1} {2:.10f}'.format(p[0], p[1], p[2]))
