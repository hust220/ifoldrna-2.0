import naspy.pdb as pdb
import sys
import re

pdbfile = sys.argv[1]
consfile = sys.argv[2]

s = pdb.Structure.fromfile(pdbfile)
ls = s[0].residues()
n = len(ls)

for line in open(consfile):
    l = re.split('\s+', line.strip())
    i, j, v = int(l[0]) - 1, int(l[1]) - 1, float(l[2])
    if i < n and j < n:
        print("{:.12f} {:.3f}".format(v, ls[i]["C4'"].distance(ls[j]["C4'"])))
