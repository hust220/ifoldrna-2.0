module load anaconda

name=$1

perl -lane 'if($#F==3){print $F[1]." ".$F[2]." ".$F[3]}' ${name}-Rep1_normalizedDels.txt >c1.txt
perl -lane 'if($#F==3){print $F[1]." ".$F[2]." ".$F[3]}' ${name}-Rep2_normalizedDels.txt >c2.txt

n=$(seq=$(head -n1 ${name}.txt);echo ${#seq})

perl -lane '$n++;if($F[0]>'$n'||$F[1]>'$n'){print "'c1.txt' line $n exceeds the residue index limit ('$n')"}' c1.txt
perl -lane '$n++;if($F[0]>'$n'||$F[1]>'$n'){print "'c2.txt' line $n exceeds the residue index limit ('$n')"}' c2.txt

python get_percentile_range.py c1.txt 95 97 >c1-95-97.txt
python get_percentile_range.py c1.txt 97 99 >c1-97-99.txt
python get_percentile_range.py c1.txt 99 99.5 >c1-99-995.txt
python get_percentile.py c1.txt 99.5 >c1-995.txt

python get_percentile_range.py c2.txt 95 97 >c2-95-97.txt
python get_percentile_range.py c2.txt 97 99 >c2-97-99.txt
python get_percentile_range.py c2.txt 99 99.5 >c2-99-995.txt
python get_percentile.py c2.txt 99.5 >c2-995.txt

python constraints.py c1-95-97.txt 50 >restraints1-95-97-50.txt
python constraints.py c1-97-99.txt 45 >restraints1-97-99-45.txt
python constraints.py c1-99-995.txt 40 >restraints1-99-995-40.txt
python constraints.py c1-995.txt 35 >restraints1-995-35.txt

cat restraints1-95-97-50.txt restraints1-97-99-45.txt restraints1-99-995-40.txt restraints1-995-35.txt >restraints1.txt

python constraints.py c2-95-97.txt 50 >restraints2-95-97-50.txt
python constraints.py c2-97-99.txt 45 >restraints2-97-99-45.txt
python constraints.py c2-99-995.txt 40 >restraints2-99-995-40.txt
python constraints.py c2-995.txt 35 >restraints2-995-35.txt

cat restraints2-95-97-50.txt restraints2-97-99-45.txt restraints2-99-995-40.txt restraints2-995-35.txt >restraints2.txt

