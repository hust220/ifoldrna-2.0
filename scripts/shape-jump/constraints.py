import sys
import re

in_file = sys.argv[1]
max_distance = float(sys.argv[2])

for line in open(in_file):
    l = re.split('\s+', line.strip())
    a, b = int(l[0]), int(l[1])
    print("SLOPE A/{0}/C4' A/{1}/C4' 0 {2} 1.0".format(a, b, max_distance))
