if [ ! -L ./data ]; then
    ln -s $SimRNA_Data ./data
fi

mkdir -p all
num_clust=$(ls *_clust*.trafl | wc -l)

for i in $(seq -w 1 ${num_clust}); do name=$(ls all_thrs*A_clust${i}.trafl); n=$(cat ${name} | wc -l); n=$((n/2)); if [[ ${n} -gt 10 ]]; then n=10; fi; echo ${name} ${n}; SimRNA_trafl2pdbs ../test_01_01-000001.pdb ${name} 1:${n} AA; done

mkdir -p all

for i in $(seq -w 1 ${num_clust}); do fullname=$(ls all_thrs*A_clust${i}.trafl); name=${fullname%.*}; n=$(cat ${name}.trafl | wc -l); n=$((n/2)); if [[ ${n} -gt 10 ]]; then n=10; fi; for j in $(seq 1 ${n}); do num=$(printf "%06d" ${j}); cp ${name}-${num}_AA.pdb all/clust${i}-$(printf "%02d" ${j}).pdb; done; done
