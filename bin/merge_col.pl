#!/usr/bin/perl

if (@ARGV<2) {
  printf("usage: merge_col.pl file1 file2 [file3]\n");
  printf(" two files has same number of lines\n");
  printf(" lines of file1 will be added to file2\n");
  printf(" if file3 is not give, file2 will be modified\n");
  exit(1);
}

open(IN,"<$ARGV[0]");
@file1 = <IN>;
close(IN);

open(IN,"<$ARGV[1]");
@file2 = <IN>;
close(IN);

my $n1 = @file1;
my $n2 = @file2;
if ($n1 != $n2) {
  printf ("CAN NOT CAT since the two files have different number of lines $n1 $n2\n");
  exit(1);
}

if (@ARGV>=3) {
  open(OUT,">$ARGV[2]");
}
else {
  open(OUT,">$ARGV[1]");
}


for (my $i=0; $i<$n1; $i++) {
  chomp($file2[$i]);
  printf OUT ("%s \t %s", $file2[$i], $file1[$i] );
}
close(OUT);
