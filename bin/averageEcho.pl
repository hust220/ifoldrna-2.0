#!/usr/bin/perl

if (@ARGV<1) {
  printf("usage: average.pl ECHOFile [window]\n");
  exit(1);
}

open (IN,"<$ARGV[0]");

my $window = 100;
if(@ARGV > 1){
 $window = $ARGV[1];
}
my @buffer;
my $index = 0;
my $pt;
my $ave = 0;
while (<IN>) {
  if (! m/^\#/ ) {
    my @tmp_array = split(" ", $_);
    if ($index<$window) {
      $buffer[$index][0] = $tmp_array[2]+$tmp_array[7];
      $buffer[$index][1] = $tmp_array[0];
      $ave +=  $buffer[$index][0];
    }
    else {
      if ($index==$window) {
	my $total = 0;
	my $n = int($window/2);
	for (my $i=0; $i<$n; $i++) {
	  $total += $buffer[$i][0];
	}
	my $tmp_int = $n;
	for (my $i=0; $i<$n; $i++) {
	  $total += $buffer[$i+$n][0];
	  $tmp_int++;
	  printf("%10.2f %f\n",$buffer[$i][1], $total/$tmp_int);
	}
      }



      $pt = $index % $window;
      my $tmp_float = $tmp_array[2]+$tmp_array[7];
      $ave -= $buffer[$pt][0];
      $ave += $tmp_float;
      printf("%10.2f %f\n",($buffer[$pt][1]+$tmp_array[0])/2, $ave/$window);
      $buffer[$pt][0] = $tmp_float;
      $buffer[$pt][1] = $tmp_array[0];
    }
    $index++;
    $pt++;
  }
}
close(IN);

{#the last chunk
  $pt--;
  my $total = 0;
  my $n = int($window/2);
  for (my $i=0; $i<$n; $i++) {
    $total += $buffer[($pt-$i)%$window][0];
  }
  my $tmp_int = $n;
  for (my $i=0; $i<$n; $i++) {
    $total += $buffer[($pt-$i-$n)%$window][0];
    $tmp_int++;
    $buffer[($pt-$i)%$window][0]=$total/$tmp_int;
  }
  #output
  for (my $i=1; $i<=$n; $i++) {
    printf("%10.2f %f\n",$buffer[($pt-$n+$i)%$window][1], $buffer[($pt-$n+$i)%$window][0]);
  }
}

