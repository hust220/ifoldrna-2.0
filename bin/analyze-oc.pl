#!/usr/bin/perl

if (@ARGV<2) {
  printf("usage: analyze-oc.pl oc-input oc-output [SIM/DIST [CUTOFF cutoff/nCLUSTER ncluster]]\n");
  printf(" SIM/DIST: if the pair-wise metric is similarity(oc-input), choose SIM;\n");
  printf("           if the metric is distance, choose DIST.\n");
  printf(" CUTOFF:   define the similarity or distance cutoff for clustering\n");
  printf(" nCLUSTER: define the number of clusters for clustering\n");
  printf("           Only either CUTOFF or nCLUSTER can be assigned.\n");
  exit(1);
}

#Read the oc-input file
#format:
#N-nodes
#name of 0th node
#name of 1st node
#....
#name of (N-1)th node
#dist/sim of 0th vs 1th
#dist/sim of 0th vs 2th
#.....
#dist/sim of 0th vs *(N-1)th
#dist/sim of 1th vs 2th
#....
#dist/sim of (N-2)th vs (N-1)th

$oc_input = $ARGV[0];
#open the file
open(OC_IN, "<$oc_input");
#N-nodes
$a = <OC_IN>;
chomp($a);
$n_nodes = $a;
#name of each node
for (my $i=0; $i<$n_nodes; $i++) {
  $a = <OC_IN>;
  chomp($a);
  $nodes[$i] = $a;
}
#dist/sim matrix
for (my $i=0; $i<$n_nodes; $i++) {
  $metric[$i][$i] = -1;
  for (my $j=$i+1; $j<$n_nodes; $j++) {
    $a = <OC_IN>;
    chomp($a);
    $metric[$j][$i] = $a;
    $metric[$i][$j] = $a;
  }
}

#Read the output file
#check the oc-manual
$oc_output = $ARGV[1];
open(OC_OUT, "<$oc_output");
$layer = 0;
$layer_cutoff[$layer] = 0;
$layer_number[$layer] = $n_nodes;
for (my $i=0; $i<$n_nodes; $i++) {
  $layer_tree[$layer][$i] = " $i";
}
$layer++;

while (<OC_OUT>) {
  chomp($_);
  my @tmp_array = split(" ", $_);
  if ($tmp_array[0] eq "##") {
    $layer_cutoff[$layer] = $tmp_array[2];
    #read out the second line
    <OC_OUT>;
    #take the newly-merged cluster
    $a = <OC_OUT>;
    #printf("%s\n",$a);
    chomp($a);
    $n_prev = $layer_number[$layer-1];
    #printf("%d\n",$n_prev);
    $i1 = -1;
    for (my $i=0; $i<$n_prev; $i++) {
      $len = length($layer_tree[$layer-1][$i]);
      if ((substr($a, 0, $len) eq $layer_tree[$layer-1][$i]) &&
	  (substr($a,$len,1) eq " ")) {
	$i1 = $i;
	$b = substr($a,$len);
	last;
      }
    }
    if ($i1<0) {
      printf("error! Can not find first cluster\n Quit\n");
      exit(1);
    }
    $i2 = -1;
    for (my $i=0; $i<$n_prev; $i++) {
      #printf("%s %s\n", $b, $layer_tree[$layer-1][$i]);
      if ($b eq $layer_tree[$layer-1][$i]) {
	$i2 = $i;
	last;
      }
    }
    if ($i2<0) {
      printf("error! Can not find second cluster: $b\n Quit\n");
      exit(1);
    }
    $layer_number[$layer] = $n_prev-1;
    #redefine the new layer_tree
    my $index=0;
    for (my $i=0; $i<$n_prev; $i++) {
      if ($i==$i1) {
	$layer_tree[$layer][$index] = $a;
	$index++;
      }
      else {
	if ($i!=$i2){
	  $layer_tree[$layer][$index] = $layer_tree[$layer-1][$i];
	  $index++;
	}
      }
    }
    #for (my $i=0; $i<$n_prev-1; $i++) {
    #  printf("%s\n",$layer_tree[$layer][$i]);
    #}

    #printf("%f %d %d\n", $layer_cutoff[$layer],$layer, $layer_number[$layer]);
    $layer++;
  }
}

if (@ARGV==2) {
  #only output the clustering information
  printf("#layer_index layer_cutoff number_of_cluster\n");
  for (my $i=0; $i<$n_nodes; $i++) {
    printf("%d %f %d\n",$i,$layer_cutoff[$i],$layer_number[$i]);
  }
}
if (@ARGV==5) {
  my $mode = 1;
  if ($ARGV[2] eq "SIM") {
    $mode = 0;
  }
  if ($ARGV[2] eq "DIST") {
    $mode = 1;
  }
  $cut_layer = 0;
  if ($ARGV[3] eq "CUTOFF") {
    $cutoff = $ARGV[4];
    for (my $i=0; $i<$n_nodes; $i++) {
      if ($mode) {#DIST
	if ($layer_cutoff[$i]<$cutoff) {
	  $cut_layer = $i;
	}
	else {
	  last;
	}
      }
      else {#SIM
	if ($layer_cutoff[$i]>$cutoff) {
	  $cut_layer = $i;
	}
	else {
	  last;
	}
      }
    }
  }
  if ($ARGV[3] eq "nCLUSTER") {
    $n_cluster = $ARGV[4];
    if (($n_cluster<=0) || ($n_cluster>$n_nodes)) {
      printf("error:The number of clusters is out of range\n");
      exit(1);
    }
    $cut_layer = $n_nodes - $n_cluster;
  }
  #printf("%d\n", $cut_layer);
  #process the clusters for the given selection
  printf("number of clusters: %d %f\n", $layer_number[$cut_layer], $layer_cutoff[$cut_layer]);
  for (my $i=0; $i<$layer_number[$cut_layer]; $i++) {
    #printf("members: %s\n",$layer_tree[$cut_layer][$i]);
    my @tmp_array = split(" ",$layer_tree[$cut_layer][$i]);
    my $cluster_size = @tmp_array;
    printf("Cluster: %d Number of Nodes: %d\n", $i, $cluster_size);
    my @sum;
    for (my $j=0; $j<$cluster_size; $j++) {
      printf("%-4d %s\n",$j,$nodes[$tmp_array[$j]]);
      $sum[$j]=0;
      for (my $k=0; $k<$cluster_size; $k++) {
	if ($k!=$j) {
	  $sum[$j] += $metric[$tmp_array[$j]][$tmp_array[$k]];
	}
      }
    }
    #find the centroid
    $the_centroid = 0;
    if ($mode) {#dist
      $min = $sum[0];
      for ($j=1; $j<$cluster_size; $j++) {
	if ($min>$sum[$j]) {
	  $the_centroid = $j;
	  $min = $sum[$j];
	}
      }
    }
    else {
      $max = $sum[0];
      for ($j=1; $j<$cluster_size; $j++) {
	if ($max<$sum[$j]) {
	  $the_centroid = $j;
	  $max = $sum[$j];
	}
      }
    }
    #print the centroid
    printf("Centroid nodes: %s Nodes: %d\n", $nodes[$tmp_array[$the_centroid]],$cluster_size);
  }
}
