#!/usr/bin/perl

if (@ARGV<2) {
  printf("usage: command.pl echoFile dN\n");
  exit(1);
}

open(IN, "<$ARGV[0]");

my $counter = 0;
my @lines;
while (<IN>) {
  chomp();
  if ($counter<$ARGV[1]) {
    $lines[$counter] = $_;
  }
  else {
    #find the lowest energy
    my $min = 10000000;
    my $Emin = 1000000;
    my $the_min = 0;
    #my $aveE = 0;
    for (my $i=0; $i<$ARGV[1]; $i++) {
      my @tmp_array = split(" ", $lines[$i]);
      #my $E = $tmp_array[2]+$tmp_array[5]+$tmp_array[7];
      my $E = $tmp_array[2]+$tmp_array[7];
      my $ener = $tmp_array[11];
      if ($ener < $min) {
	$min = $ener;
	$the_min = $i;
      }
      if ($E < $Emin) {
	$Emin = $E;
      }
      #$aveE += $E;
    }
    #$Emin = $aveE/$ARGV[1];
    printf("%s %f\n", $lines[$the_min], $Emin);
    $counter=0;
    $lines[$counter] = $_;
  }
  $counter++;
}
close(IN);
