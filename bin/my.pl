#!/usr/bin/perl


if (@ARGV<5) {
  printf("usage: analyze.pl pdb seq aveReactivity length nREPLICA CLUSTERS\n");
  exit(1);
}

#get the path
my $PATH;
{
  my @tmp_array = split(/my.pl/, $0);
  $PATH = $tmp_array[0];
}

$getRMSD = $PATH."movie2rmsd.linux";
$getCC   = $PATH."movie2NCcorr.linux";
$merge   = $PATH."merge_col.pl";
$sparse  = $PATH."sparse-echo-1.pl";
$getENS  = $PATH."ensemble.pl";
$mov2pdb = $PATH."nseq_movie2pdb.linux";
$getPDB  = $PATH."getPDB.sh";
$getCLUS = $PATH."getCLUSTER.sh";
$anaCLUS = $PATH."analyze-oc.pl";
#print $getRMSD ."\n";
#print $getCC . "\n";
#exit(1);


$nENS1 = $ARGV[4]-1;
#
#generate RMSD file
#

#
#generate CC file
#


#
#merge_col
#

$com = "for i in `seq -w 0 $nENS1`; do grep -v \"#\" p\*\$\{i\}.\*.echo > p\$\{i\}.ener;  $merge p\$\{i\}.rmsd p\$\{i\}.ener; $merge p\$\{i\}.cc p\$\{i\}.ener; $sparse  p\$\{i\}.ener 10 > p\$\{i\}.ener.1; done";
print $com . "\n";
system($com);

#
#generate the movies
#

