#!/usr/bin/perl

if (@ARGV<1) {
  printf("usage: genOCInput.pl list \n");
  exit(1);
}

my $PATH;
{
  my @tmp_array = split(/genOCInput.pl/, $0);
  $PATH = $tmp_array[0];
}

$rmsd = $PATH."getRMSD.linux";

open(IN,"<$ARGV[0]");
my $iline=0;
my @nodes;
while (<IN>) {
  chomp($_);
  $nodes[$iline++] = $_;
}

printf("%d\n",$iline);
for (my $i=0; $i<$iline; $i++) {
  printf("%s\n",$nodes[$i]);
}

for (my $i=0; $i<$iline; $i++) {
  $inode = $nodes[$i];
  for (my $j=$i+1; $j<$iline; $j++) {
    $jnode = $nodes[$j];
    my $rms =`$rmsd $inode $jnode`;
    chomp($rms);
    printf("%f\n",$rms);
  }
}
