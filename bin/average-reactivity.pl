#!/usr/bin/perl

if (@ARGV<2) {
  printf("usage: average.pl reactivityData length\n");
  exit(1);
}

my $size = $ARGV[1];
my @data;
for (my $i=0; $i<$size+1; $i++) {
  $data[$i] = -999;
}

open(IN,"<$ARGV[0]");
while (<IN>) {
  chomp();
  my @tmp_array = split(" ",$_);
  my $index = $tmp_array[0]-1;
  my $react = $tmp_array[1];
  if ($index>=0 && $index<$size) {
    $data[$index] = $react;
  }
}
close(IN);

for (my $i=0; $i<$size; $i++) {
  if ($data[$i]==-999 && ($i>0 && $data[$i-1]>-999) && ($i+1<$size && $data[$i+1]>-999)) {
    $data[$i] = ($data[$i-1]+$data[$i+1])/2;
  }
}

for (my $i=0; $i<$size;$i++) {
  if ($data[$i]>-999) {
    my $prev = $data[$i];
    if ($i>0 && $data[$i-1]>-999) {
      $prev = $data[$i-1];
    }
    my $next = $data[$i];
    if ($i+1<$size && $data[$i+1]>-999) {
      $next = $data[$i+1];
    }
    printf("%d %f\n",$i+1,($next+$data[$i]+$prev)/3);
  }
}
