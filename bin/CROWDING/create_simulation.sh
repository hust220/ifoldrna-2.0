#!/bin/sh

# give mass of crowder and radius

mass=20
radius=5

if [ $# -ne 2 ]; 
then
echo "Usage: source create_simulation.sh pdb.file boxsize"
return 
fi

if ! [ -a $1 ]; 
then 
echo "pdb file doesn't exist"; 
echo "Usage: source create_simulation.sh pdb.file boxsize"
return
fi 

/nas02/home/k/r/krohotin/HRP-1.0/bin/pdb2txt.linux $1 $2 > pre.txt 

mkdir noc
cp pre.txt noc/crow.txt 
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/start noc/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/start-20.rex noc/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/mycode.bsub noc/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/copy.sh noc/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/cutcolumn noc/

cd noc/
bsub < mycode.bsub
cd ../

nspheres=('343' '512' '729' '1000' '1331' '1728' '2197' '2744' '3375' '4096' '4913' '5832' '6859' '8000');  
for ((i=0; i<=13; i++)); do
mkdir m${mass}_r${radius}_${nspheres[$i]}

# create txt file for simulation
echo "data_to_read:  pre.txt" > CROW  
echo "file_to_write: crow.txt" >> CROW 
echo "mass: " $mass >> CROW
echo "radius: " $radius >> CROW
echo "nspheres: " ${nspheres[$i]} >> CROW
/nas02/home/k/r/krohotin/HRP-1.0/bin/addcrowder_v1 CROW
mv crow.txt m${mass}_r${radius}_${nspheres[$i]}/

cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/start m${mass}_r${radius}_${nspheres[$i]}/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/start-20.rex m${mass}_r${radius}_${nspheres[$i]}/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/mycode.bsub m${mass}_r${radius}_${nspheres[$i]}/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/copy.sh m${mass}_r${radius}_${nspheres[$i]}/
cp /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/cutcolumn m${mass}_r${radius}_${nspheres[$i]}/

cd m${mass}_r${radius}_${nspheres[$i]}/
bsub < mycode.bsub 
cd ../

done