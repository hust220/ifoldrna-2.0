# makes directory with energies and check for integrity of energy files

# give mass of crowder and radius
# mdsteps should match expected number of entries in echo file

mdsteps=200000
mass=20
radius=5


if [ $# -ne 1 ]; 
then
echo "Usage: source mkenergy.sh dir_name"
return 
fi

if ! [ -a $1 ]; 
then 
echo "dir_name file doesn't exist"; 
echo "Usage: source mkenergy.sh dir_name"
return
fi 


nspheres=('343' '512' '729' '1000' '1331' '1728' '2197' '2744' '3375' '4096' '4913' '5832' '6859');
conc=('5' '10' '15' '20' '25' '30' '35' '40' '45' '50');

cd $1

mkdir $1

#for ((i=0; i<=-1; i++)); do
for ((i=0; i<=12; i++)); do
cd  m${mass}_r${radius}_${nspheres[$i]}
source copy.sh 
mv data m${mass}_r${radius}_${nspheres[$i]}
cd m${mass}_r${radius}_${nspheres[$i]}
  touch temp
  for ((j=0; j<=19; j++)); do
  l=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/st.sh < energy$j`
  echo $l>>temp
  done 
  minval=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/min.sh < temp` 
  if [ $minval -eq $mdsteps ]; 
  then 
   echo m${mass}_r${radius}_${nspheres[$i]}  OK!
  fi
  if [ $minval -lt $mdsteps ]; 
  then 
   echo m${mass}_r${radius}_${nspheres[$i]}  BAD!  $minval 
   for ((j=0; j<=19; j++)); do
   cat energy$j | head -$minval > energy${j}_n
   mv energy${j}_n energy$j
   done
  fi 
cd ../
mv m${mass}_r${radius}_${nspheres[$i]} ../$1/
cd ../ 
done

#for ((i=0; i<=-1; i++)); do
for ((i=0; i<=9; i++)); do
cd m${mass}_r${radius}_c${conc[$i]} 
source copy.sh
mv data m${mass}_r${radius}_c${conc[$i]}
cd m${mass}_r${radius}_c${conc[$i]}
  touch temp
  for ((j=0; j<=19; j++)); do
  l=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/st.sh < energy$j`
  echo $l>>temp
  done
  minval=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/min.sh < temp`
  if [ $minval -eq $mdsteps ];
  then
   echo m${mass}_r${radius}_c${conc[$i]}  OK!
  fi
  if [ $minval -lt $mdsteps ];
  then
   echo m${mass}_r${radius}_c${conc[$i]}  BAD!  $minval
   for ((j=0; j<=19; j++)); do
   cat energy$j | head -$minval > energy${j}_n
   mv energy${j}_n energy$j
   done
  fi
cd ../
mv m${mass}_r${radius}_c${conc[$i]} ../$1/
cd ../
done

cd noc/
source copy.sh
mv data noc 
cd noc/
touch temp 
  for ((j=0; j<=19; j++)); do
  l=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/st.sh < energy$j`
  echo $l>>temp
  done
  minval=`source /nas02/home/k/r/krohotin/HRP-1.0/bin/CROWDING/min.sh < temp`
  if [ $minval -eq $mdsteps ];
  then
   echo noc  OK!
  fi
  if [ $minval -lt $mdsteps ];
  then
   echo noc  BAD!  $minval
   for ((j=0; j<=19; j++)); do
   cat energy$j | head -$minval > energy${j}_n
   mv energy${j}_n energy$j
   done
  fi
cd ../
mv noc ../$1/
cd ../

cd ../