#!/bin/bash

if [ $# -ne 2 ];
then
echo "Usage: source write.sh energy_file_name nlines"
return
fi

filename=$1

touch ${filename}_n


i=0
while read line 
do
let i++
if [ $i -le $2 ]; 
then
 echo $line >> ${filename}_n  
fi
done < ${filename}


