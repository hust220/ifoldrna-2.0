
#!/bin/sh

if [ $# -ne 2 ]; 
then
echo "Usage: source create_simulation.sh pdb.file boxsize"
return 
fi

if ! [ -a $1 ]; 
then 
echo "pdb file doesn't exist"; 
echo "Usage: source create_simulation.sh pdb.file boxsize"
return
fi 

/nas02/home/k/r/krohotin/HRP-1.0/bin/pdb2txt.linux $1 $2 > pre.txt 

nspheres=('343' '512' '729' '1000' '1331' '1728' '2197' '2744' '3375' '4096' '4913' '5832' '6859' '8000');  
for ((i=0; i<=13; i++)); do

done