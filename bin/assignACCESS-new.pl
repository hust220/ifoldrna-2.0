#!/usr/bin/perl

if (@ARGV<2) {
    printf("usage: assignACCESS.pl reactivityFile basePairs rnaLength [no_average_data]\n");
    printf(" !!!It is ADVISED to use the raw, normalized reactivity data.!!!\n");
    printf(" reactivity format: two columns(index reactivity)\n");
    printf(" NOTE:: no_average_data is set NOT to average the activity\n");
    exit(1);
}

my $N = $ARGV[2];
$MAX = -1000000;
$maxN = 11.0;
$minN = 0.50;
#if ($N<100) {
#  $maxN = 9.0;
#}

$minE = -0.100;
$midE = -0.070;
$maxE = -0.050;

my @access;

my @data;
for (my $i=0; $i<$N+1; $i++) {
    $data[$i] = -999;
}


open(IN,"<$ARGV[0]");
my $max_num = 0;
while (<IN>) {
    chomp();
    my @tmp = split(" ",$_);
    my $index = $tmp[0]-1;
    my $react = $tmp[1];
    if ($index<0 || $index>=$N) {
        printf("error: out of range\n");
        exit(1);
    }
    if($react < 100 && $react > -100){
        $access[$index] = $react;
        $data[$index] = $react;
        if ($react>$MAX) {
            $MAX = $react;
        }
        if ($index>$max_num) {
            $max_num = $index;
        }
    }
}
close(IN);
$max_num++;
if ($max_num>$N) {
    $max_num = $N;
}

for (my $i=0; $i<$N; $i++) {
    if ($data[$i]==-999 && ($i>0 && $data[$i-1]>-999) && ($i+1<$N && $data[$i+1]>-999)) {
        $data[$i] = ($data[$i-1]+$data[$i+1])/2;
        #printf("%d %f\n",$i+1, $data[$i]);
    }
}

my $AVERAGE=1;
if (@ARGV>3) {
    $AVERAGE=0;
}


my @JUNK;
for (my $i=0; $i<$N;$i++) {
    if ($data[$i]>-999) {
        my $prev = $data[$i];
        if ($i>0 && $data[$i-1]>-999) {
            $prev = $data[$i-1];
        }
        my $next = $data[$i];
        if ($i+1<$N && $data[$i+1]>-999) {
            $next = $data[$i+1];
        }
        $JUNK[$i]=($next+$data[$i]+$prev)/3;
    }
}

my @my_data;
for (my $i=0; $i<$N; $i++) {
    if (!$AVERAGE) {
        $my_data[$i] = $data[$i];
    }
    if ($data[$i]>-999) {
        $data[$i] = $JUNK[$i];
    }
}

open(IN, "<$ARGV[1]");
my @basepair;
for (my $i=0; $i<$N; $i++) {
    $basepair[$i] = -100;
}
while (<IN>) {
    chomp();
    my @tmp = split(" ",$_);
    my $i = substr($tmp[0],1);
    my $j = substr($tmp[1],1);
    #printf("%d %d\n",$i,$j);
    $basepair[$i-1] = $j-1;
    $basepair[$j-1] = $i-1;
}
close(IN);
#exit(1);


my @tmp_access;
my @tmp_data;
my $tmp_idx=0;
my $tmp_didx=0;
#print $N;
#printf STDERR ("N $N\n");
for (my $i=0; $i<$N; $i++) {
#printf STDERR ("i $i\n");
    if (defined($access[$i])) {
        $tmp_access[$tmp_idx] = $access[$i];
        $tmp_idx++;
        $tmp_data[$tmp_didx] = $data[$i];
        $tmp_didx++;
    }
    else {
        $access[$i] = $MAX+1000;
        if ($data[$i]>-999) {
            $tmp_data[$tmp_didx] = $data[$i];
            $tmp_didx++;
        }
    }
}
#printf STDERR ("tmp_didx: $tmp_didx\n");

my @sorted_access = sort {$a <=> $b} @tmp_access;
my @sorted_data = sort {$a <=> $b} @tmp_data;

for (my $i=0; $i<$N; $i++) {
    if ($data[$i]<=-999) {
        $data[$i] = $MAX+1000;
    }
}

#my $SUM_access=0;
#my $iSUM=0;
#my $END  = int(0.02*$tmp_idx);
#for (my $i=$tmp_idx-$nEXP; $i<$tmp_idx-$END; $i++) {
#  $iSUM++;
#  $SUM_access+= $sorted_access[$i];
#}
#$SUM_access /=$iSUM;
#printf STDERR ("min_access: %f max_ccess: %f\n", $min_access, $SUM_access);
#if (abs($SUM_access-1.0)>0.2) {
#  printf("NOT normalized data. Please check\n");
#  exit(1);
#}
#$max_access = $SUM_access;
my @burriness;

my $putative_BUR  =  $sorted_access[int($tmp_idx*0.25)];
my $putative_hBUR  = ($sorted_access[int($tmp_idx*0.30)]+$sorted_access[int($tmp_idx*0.50)])/2;
my $putative_INTER = $sorted_access[int($tmp_idx*0.80)];
my $prev = -1;
my $prev_core = -8;

my @core_res;
my $icore=0;;
printf STDERR ("BUR: %f %f %f\n",$putative_BUR, $putative_hBUR, $putative_INTER);
#create the tentative bases;
for (my $i=0; $i<$N; $i++) {
    if ($access[$i]<=$putative_hBUR) {
        $CG_access[$i] = -1;
    }
    else {
        $CG_access[$i] = 0;
        if ($access[$i]>$putative_INTER) {
            $CG_access[$i] = 1;
        }
        if ($access[$i]>$MAX) {
            $CG_access[$i] = 2;
        }
    }
}
$prev=-1;
for (my $i=0; $i<$N; $i++) {
    if ($CG_access[$i]<0) {
        if ($prev<0) {
            $prev = $i;
        }
    }
    else {
        if ($prev>=0) {
            my $diff = $prev-$i;
            my $has_low = 0;
            for(my $j=$prev; $j<$i; $j++){
                if($access[$j]<=$putative_BUR){
                    $has_low = 1;
                }
            }
            if($has_low){
                for (my $j=$prev; $j<$i; $j++) {
                    $CG_access[$j] = $diff;
                }
                #printf STDERR ("$prev $i \n");
            }
            $prev = -1;
        }
    }
}
for (my $i=1; $i<$N-1; $i++) {
    if (($CG_access[$i]==0 && ($CG_access[$i-1]<-1 || $CG_access[$i+1]<-1))) {
        $CG_access[$i] = -1;
        #printf STDERR ("FIX: %d %d %d\n",$i+1,$CG_access[$i-1], $CG_access[$i+1]);
    }
    if ((($CG_access[$i]==0) && $CG_access[$i-1]<0 && $CG_access[$i+1]<0)){
        my $sum = $CG_access[$i-1]+$CG_access[$i+1];
        if ($sum<=-3) {
            $CG_access[$i] = $sum;
        }
    }
    if($CG_access[$i]>1 && (($access[$i-1]<$putative_BUR || $access[$i+1]<$putative_BUR) && ($CG_access[$i-1]<=0 && $CG_access[$i+1]<=0)) ){
        $CG_access[$i] = -1;
        #printf STDERR ("FIX: %d %d %d\n",$i+1,$CG_access[$i-1], $CG_access[$i+1]);
    }
}
$prev=-1;
for (my $i=0; $i<$N; $i++) {
    if ($CG_access[$i]<0) {
        if ($prev<0) {
            $prev = $i;
        }
    }
    else {
        if ($prev>=0) {
            my $diff = $prev-$i;
            if ($diff <= -3) {
                for (my $j=$prev; $j<$i; $j++) {
                    $CG_access[$j] = $diff;
                }
            }
            else {
                for (my $j=$prev; $j<$i; $j++) {
                    $CG_access[$j] = 0;
                }
            }
            $prev = -1;
        }
    }
}

$prev=-1;
for(my $i=5; $i<$max_num;$i++) {
    #if ($access[$i]<$putative_hBUR) {
    if ($CG_access[$i]<0 && $i<$max_num-1) {
        if ($prev<0) {
            $prev = $i;
        }
    }
    else {
        if ($prev>=0) {
            if (($i-$prev)>=3) {
                my $diff=$i-$prev;
                my $nn = int($diff/10);
                if ($nn==0) {
                    $nn=1;
                }
                my $step = $diff/$nn;
                for (my $istep=0; $istep<$nn; $istep++) {
                    my $min = $MAX+100;
                    my $min_idx;
                    my $start = $prev+$istep*$step+1;
                    if ($istep>0) {
                        $start-=1;
                    }
                    my $end = $prev+($istep+1)*$step-1;
                    if ($istep<$nn-1) {
                        $end+=1;
                    }
                    for (my $j=$start; $j<$end; $j++) {
                        if ($access[$j]<$min) {
                            $min = $access[$j];
                            $min_idx = $j;
                        }
                    }
                    #printf STDERR ("$prev $i $min\n");
                    if ($min<=$putative_BUR ||
                        $access[$min_idx-1]<=$putative_BUR ||
                        $access[$min_idx+1]<=$putative_BUR ) {
                        #printf STDERR ("%d %d\n", $min_idx+1, $CG_access[$min_idx]);
                        $core_res[$icore] = $min_idx;
                        $icore++;
                    }
                }
            }
            $prev = -1;
        }
    }
}

#check whether two cores are close to each other
my @nNeighbors;
for (my $i=0; $i<$N; $i++) {
    $nNeighbors[$i] = 0;
}

for (my $i=0; $i<@core_res; $i++) {
    my $icore = $core_res[$i];
    if ($icore<0) {
        $icore *= -1;
    }
    for (my $j=$i+1; $j<@core_res; $j++) {
        my $isNeighbor=0;
        my $jcore = $core_res[$j];
        if ($jcore<0) {
            $jcore *= -1;
        }
        if ($basepair[$icore]>=0) {
            if ($basepair[$jcore]>=0) {
                if (($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 )||
                    ($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )||
                    (abs($basepair[$icore]-$basepair[$jcore])<=2)  ) {
                    #printf("1-neighbors: %d %d\n",$icore+1,$jcore+1);
                    $isNeighbor=1;
                    my $iave = ($access[$icore-1]+$access[$icore]+$access[$icore+1])/3;
                    my $jave = ($access[$jcore-1]+$access[$jcore]+$access[$jcore+1])/3;
                    if ($icore<$jcore) {
                        $core_res[$j] = -abs($jcore);
                        $core_res[$i] = abs($icore);
                    }
                    else {
                        $core_res[$i] = -abs($icore);
                        $core_res[$j] = abs($jcore);
                    }
                    if (!($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 )&&
                        !($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )) {
                        $nNeighbors[$icore]++;
                        $nNeighbors[$jcore]++;
                    }
                }
            }
            else {
                if (($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 ) ||
                    ($basepir[$jcore-1]>=0 && abs($basepair[$jcore-1]-$basepair[$icore])<=1) ||
                    ($basepir[$jcore+1]>=0 && abs($basepair[$jcore+1]-$basepair[$icore])<=1) ){
                    #printf("2-neighbors: %d %d\n",$icore+1, $jcore+1);
                    $isNeighbor=1;
                    my $iave = ($access[$icore-1]+$access[$icore]+$access[$icore+1])/3;
                    my $jave = ($access[$jcore-1]+$access[$jcore]+$access[$jcore+1])/3;
                    if ($icore<$jcore) {
                        $core_res[$i] = abs($icore);
                        $core_res[$j] = -abs($jcore);
                    }
                    else {
                        $core_res[$i] = -abs($icore);
                        $core_res[$j] = abs($jcore);
                    }
                    if (!($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 )) {
                        $nNeighbors[$icore]++;
                        $nNeighbors[$jcore]++;
                    }
                }
            }
        }
        else {
            if ($basepair[$jcore]>=0) {
                if (($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 ) ||
                    ($basepir[$icore-1]>=0 && abs($basepair[$icore-1]-$basepair[$jcore])<=1) ||
                    ($basepir[$icore+1]>=0 && abs($basepair[$icore+1]-$basepair[$jcore])<=1) ) {
                    #printf("3-neighbors: %d %d\n", $icore+1, $jcore+1);
                    $isNeighbor=1;
                    my $iave = ($access[$icore-1]+$access[$icore]+$access[$icore+1])/3;
                    my $jave = ($access[$jcore-1]+$access[$jcore]+$access[$jcore+1])/3;
                    if ($icore<$jcore) {
                        $core_res[$i] = abs($icore);
                        $core_res[$j] = -abs($jcore);
                    }
                    else {
                        $core_res[$i] = -abs($icore);
                        $core_res[$j] = abs($jcore);
                    }
                    if (!($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )) {
                        $nNeighbors[$icore]++;
                        $nNeighbors[$jcore]++;
                    }
                }
            }
            else {
                for (my $k=$icore-2; $k<=$icore+2; $k++) {
                    if ($basepair[$k]>=0){
                        my $tmp=($k-$icore)*($jcore-$basepair[$k]);
                        if ($tmp>=0 && $tmp<=2) {
                            $isNeighbor=1;
                            #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
                        }
                    }
                }
                for (my $k=$jcore-2; $k<=$jcore+2; $k++) {
                    if ($basepair[$k]>=0){
                        my $tmp=($k-$jcore)*($icore-$basepair[$k]);
                        if ($tmp>=0 && $tmp<=2) {
                            $isNeighbor=1;
                            #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
                        }
                    }
                }
                if ($isNeighbor) {
                    #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
                    my $iave = ($access[$icore-1]+$access[$icore]+$access[$icore+1])/3;
                    my $jave = ($access[$jcore-1]+$access[$jcore]+$access[$jcore+1])/3;
                    if ($icore<$jcore) {
                        $core_res[$i] = abs($icore);
                        $core_res[$j] = -abs($jcore);
                    }
                    else {
                        $core_res[$i] = -abs($icore);
                        $core_res[$j] = abs($jcore);
                    }
                    $nNeighbors[$icore]++;
                    $nNeighbors[$jcore]++;
                }
            }
        }
    }
}

for (my $i=0; $i<$N; $i++) {
    if ($nNeighbors[$i]>0) {
        printf STDERR ("%d has %d neightbors\n",$i+1, $nNeighbors[$i]);
    }
}

for (my $i=0; $i<@core_res; $i++) {
    if ($core_res[$i]>=0) {
        printf STDERR ("core resi: %d\n", $core_res[$i]+1);
    }
    else {
        printf STDERR ("core resi: %d (shared)\n", -$core_res[$i]+1);
    }
}

#exit(1);

for (my $i=0; $i<@core_res; $i++) {
    my $min_idx = $core_res[$i];
    if ($min_idx>=0) {
        $burriness[$min_idx-1][0] = $maxN;
        $burriness[$min_idx-1][1] = ($minE+$midE)/2;
        #$burriness[$min_idx-1][1] = $midE;
        $burriness[$min_idx-1][2] = -$minE+0.20;

        $burriness[$min_idx][0] = $maxN;
        $burriness[$min_idx][1] = $minE;
        $burriness[$min_idx][2] = -$minE+0.20;

        $burriness[$min_idx+1][0] = $maxN;
        $burriness[$min_idx+1][1] = ($minE+$midE)/2;
        #$burriness[$min_idx+1][1] = $midE;
        $burriness[$min_idx+1][2] = -$minE+0.20;
    }
    else {
        $min_idx *=-1;
        $burriness[$min_idx-1][0] = $maxN;
        $burriness[$min_idx-1][1] = ($maxE+$midE)/2;
        #$burriness[$min_idx-1][1] = $midE;
        $burriness[$min_idx-1][2] = -$minE+0.20;

        $burriness[$min_idx][0] = $maxN;
        $burriness[$min_idx][1] = ($minE+$midE)/2;
        #$burriness[$min_idx][1] = $midE;
        $burriness[$min_idx][2] = -$minE+0.20;

        $burriness[$min_idx+1][0] = $maxN;
        $burriness[$min_idx+1][1] = ($maxE+$midE)/2;
        #$burriness[$min_idx+1][1] = $midE;
        $burriness[$min_idx+1][2] = -$minE+0.20;
    }
}

#DETERMINE the EXPOSED residues
my $putative_EXP =  $sorted_access[int($tmp_idx*0.80)];
my $putative_hEXP = $sorted_access[int($tmp_idx*0.60)];
$putative_INTER = $sorted_access[int($tmp_idx*0.35)];
printf STDERR ("EXP: %f %f %f\n",$putative_EXP, $putative_hEXP, $putative_INTER);
for (my $i=0; $i<$N; $i++) {
    $CG_access[$i] = -100;
    if ($access[$i]<=$MAX) {
        if ($access[$i]>=$putative_EXP) {
            $CG_access[$i] = 2;
        }
        else {
            if ($access[$i]>=$putative_hEXP) {
                $CG_access[$i] = 1;
            }
            else {
                if ($access[$i]>=$putative_INTER) {
                    $CG_access[$i] = 0;
                }
                else {
                    $CG_access[$i] = -1;
                }
            }
        }
    }
}

for (my $i=0; $i<$N; $i++) {
    #print $i+1 .  " " . $access[$i] . " " . $CG_access[$i] . " : ";
    if ($CG_access[$i]==0 && $CG_access[$i-1]>0 && $CG_access[$i+1]>0){
        $CG_access[$i]++;
    }
    if($CG_access[$i]==-1 && 
        ($CG_access[$i-1]>1  || $CG_access[$i+1]>1 ) &&
        $CG_access[$i-1]>=0 && $CG_access[$i+1]>=0 ) {
        $CG_access[$i]=1;
    }
    #print $i+1 . " " . $CG_access[$i] . "\n";
}

$prev = -1;
my @exp_res;
my $iexp=0;
for (my $i=1; $i<$max_num; $i++) {
    if ($CG_access[$i]>0 && $i<$max_num-1) {
        if ($prev<0) {
            $prev = $i;
        }
    }
    else {
        if ($prev>=0) {
            if (($i-$prev)>=3) {#possible
                my $theMax = -100;
                for (my $j=$prev; $j<$i; $j++) {
                    if ($theMax<$CG_access[$j]) {
                        $theMax = $CG_access[$j];
                    }
                }
                if ($theMax>=2) {
                    #printf STDERR ("%d %d\n",$prev+1, $i);
                    my $the_max=-10000;
                    my $imax;
                    for (my $k=$prev+1; $k<$i-1; $k++) {
                        if ($access[$k]>$the_max) {
                            $the_max = $access[$k];
                            $imax    = $k;
                        }
                    }
                    if ($iexp>0) {
                        if ($imax-$exp_res[$iexp-1]<8) {
                            if ($access[$imax]>$access[$exp_res[$iexp-1]]) {
                                $exp_res[$iexp-1] = $imax;
                            }
                        }
                        else {
                            $exp_res[$iexp] = $imax;
                            $iexp++;
                        }
                    }
                    else {
                        $exp_res[$iexp] = $imax;
                        $iexp++;
                    }
                }
            }
            $prev = -1;
        }
    }
}

for (my $i=0; $i<$iexp; $i++) {
    printf STDERR ("exp: %d\n", $exp_res[$i]+1);
    my $max_idx = $exp_res[$i];
    $burriness[$max_idx][0]   = $minN;
    $burriness[$max_idx][1]   = $maxE;
    #$burriness[$max_idx][2]   = -$minE+0.20;
    $burriness[$max_idx][2]   = -$minE+0.20;
}


my $putative_burr = $sorted_data[int($tmp_didx*0.2)];
my $putative_exp  = $sorted_data[int($tmp_didx*0.5)];
my $nBUR = int(0.20*$tmp_didx);
my $nEXP = int(0.20*$tmp_didx);
my $nDISCARD = int(0.02*$tmp_didx);
#my $min_access = $sorted_data[$nBUR];
my $min_access=0;
my $ncount=0;
for (my $i=$nDISCARD; $i<$nBUR; $i++) {
    $ncount++;
    $min_access+=$sorted_data[$i];
}

if ($nCount == 0) {
    printf STDERR ("nCount: 0\n");
    exit(0);
}

$min_access /= $ncount;
#my $max_access = $sorted_data[$tmp_didx - $nEXP];
my $max_access = 0;
$ncount=0;
for (my $i=$tmp_didx - $nDISCARD-1; $i>=$tmp_didx-$nEXP; $i--) {
    $ncount++;
    $max_access += $sorted_data[$i];
}
$max_access /= $ncount;


my $mid_data = ($max_access+$min_access)/2.0;
my $total_bur = 0;
for (my $i=0;$i<@sorted_data; $i++) {
    if ($sorted_data[$i]<=$mid_data) {
        $total_bur++;
    }
}
$total_bur /= $tmp_didx;
printf STDERR ("burr percentage: %f\n",$total_bur);


if (!$AVERAGE) {
    $putative_burr = $sorted_access[int($tmp_idx*0.2)];
    $putative_exp  = $sorted_access[int($tmp_idx*0.5)];
    $nBUR = int(0.20*$tmp_idx);
    $nEXP = int(0.20*$tmp_idx);
    $nDISCARD = int(0.02*$tmp_idx);

    $min_access=0;
    $ncount=0;
    for (my $i=$nDISCARD; $i<$nBUR; $i++) {
        $ncount++;
        $min_access+=$sorted_access[$i];
    }
    $min_access /= $ncount;

    $max_access = 0;
    $ncount=0;
    for (my $i=$tmp_idx - $nDISCARD-1; $i>=$tmp_idx-$nEXP; $i--) {
        $ncount++;
        $max_access += $sorted_access[$i];
    }
    $max_access /= $ncount;

    for (my $i=0; $i<$N; $i++) {
        if($data[$i]<=$MAX){
            $data[$i] = $my_data[$i];
        }
    }

    $mid_data = ($max_access+$min_access)/2.0;
    $total_bur = 0;
    for (my $i=0;$i<@sorted_access; $i++) {
        if ($sorted_access[$i]<$mid_data) {
            $total_bur++;
        }
    }
    $total_bur /= $tmp_idx;
    printf STDERR ("burr percentage: %f\n",$total_bur);

}
#make sure the min_access is at least max_acces/10
if ($min_access < $max_access*0.1) {
    $min_access = $max_access*0.1;
    $mid_data = ($max_access+$min_access)/2.0;
    $total_bur = 0;
    for (my $i=0;$i<@sorted_access; $i++) {
        if ($sorted_access[$i]<$mid_data) {
            $total_bur++;
        }
    }
    $total_bur /= $tmp_idx;
    printf STDERR ("burr percentage: %f\n",$total_bur);
}

while ($total_bur>=0.55) {
    $max_access *= 0.95;
    #$min_access = $max_access*0.1;
    $mid_data = ($max_access+$min_access)/2.0;
    $total_bur = 0;
    for (my $i=0;$i<@sorted_access; $i++) {
        if ($sorted_access[$i]<$mid_data) {
            $total_bur++;
        }
    }
    $total_bur /= $tmp_idx;
    printf STDERR ("burr percentage: %f\n",$total_bur);
}

printf STDERR ("%f %f %f %f\n", $putative_burr, $putative_exp, $min_access, $max_access);


for (my $i=0; $i<$N; $i++) {
    my $maxNeighbors;
    my $attr;
    my $rep;
    #if($access[$i]<=$putative_burr && ($i>0 && $access[$i-1]>$putative_exp) && ($i<$N-1 && $access[$i+1]>$putative_exp)){
    #  #printf("ERROR prone: %d %f\n",$i+1, $access[$i]);
    #  $access[$i] = ($access[$i-1]+$access[$i]+$access[$i+1])/3.0;
    #}
    if($data[$i]<=$putative_burr && ($i>0 && $data[$i-1]>$putative_exp) && ($i<$N-1 && $data[$i+1]>$putative_exp)){
        #printf("ERROR prone: %d %f\n",$i+1, $access[$i]);
        $data[$i] = ($data[$i-1]+$data[$i]+$data[$i+1])/3.0;
    }


    if (!defined($burriness[$i][0])) {
        #printf STDERR ("HI $i %f %f %f -- %f\n",$data[$i],$min_access,$max_access, $MAX);
        #if ($access[$i]<=$min_access) {
        if ($data[$i]<=$min_access) {
            $maxNeighbors = $maxN;
            $attr = $midE;
            #$rep = -$minE+0.20;
            $rep = -$minE;
        }
        else {
            #if ($access[$i]>$MAX) {#un-defined
            if ($data[$i]>$MAX) {#un-defined
                $maxNeighbors = 0;
                $attr = $maxE;
                $rep = -$minE;
            }
            else {
                #if ($access[$i]>=$max_access) {#exposed
                if ($data[$i]>=$max_access) {#exposed
                    $maxNeighbors = $minN+0.5;
                    $attr = $maxE;
                    $rep  = -$minE;
                }
                else {#in-between
                    #$maxNeighbors = $maxN - ($access[$i]-$min_access)/($max_access-$min_access)*($maxN-$minN);
                    $maxNeighbors = $maxN - ($data[$i]-$min_access)/($max_access-$min_access)*($maxN-$minN);
                    #if ($maxNeighbors<6) {
                    #$maxNeighbors=6;
                    #$attr = $midE + ($access[$i]-$min_access)/($max_access-$min_access)*($maxE-$midE);
                    $attr = $midE + ($data[$i]-$min_access)/($max_access-$min_access)*($maxE-$midE);
                    $rep = -$attr;
                    #}
                    #else {
                    #  #$maxNeighbors=6;
                    #  $attr = $midE + ($access[$i]-$min_access)/($max_access-$min_access)*($maxE-$midE);
                    #  $rep = 0.10;
                    #}
                }
            }
        }
        $burriness[$i][0] = $maxNeighbors;
        $burriness[$i][1] = $attr;
        $burriness[$i][2] = $rep;
    }
    else {
        #if ($access[$i]>$min_access && $burriness[$i][0]>$minN && $access[$i]<$MAX){#in-between
        #printf STDERR ("%d %f %f\n",$i+1, $data[$i], $burriness[$i][0]);
        if ($data[$i]>$min_access && $data[$i]<$max_access && $burriness[$i][0]>$minN && $data[$i]<$MAX){#in-between
            #$maxNeighbors = $maxN - ($access[$i]-$min_access)/($max_access-$min_access)*($maxN-$minN);
            $maxNeighbors = $maxN - ($data[$i]-$min_access)/($max_access-$min_access)*($maxN-$minN);
            #$attr = $minE;
            #$rep = -$minE+0.20;
            $burriness[$i][0] = $maxNeighbors;
            #$burriness[$i][1] = $attr;
            #$burriness[$i][2] = $rep;
            #printf("--- %d %f\n",$i+1, $maxNeighbors);
        }
        #printf STDERR ("%d %f %f\n",$i+1, $data[$i], $burriness[$i][0]);
        if($data[$i]>$max_access && $burriness[$i][0]>$minN){
            $burriness[$i][0] = $minN;
            $burriness[$i][1] = $maxE;
            $burriness[$i][2] = -$maxE;
        }
        #if ($data[$i]<$min_access && ($burriness[$i][0]>$minN && $data[$i]<$MAX)) {
        #  $maxNeighbors = $maxN - ($data[$i]-$min_access)/($max_access-$min_access)*($maxN-$minN)*0.5;
        #  #printf STDERR ("%d %f %f\n",$i+1, $maxN, $maxNeighbors)
        #  if ($maxNeighbors>14) {
        #    $maxNeighbors = 14;
        #  }
        #  $burriness[$i][0] = $maxNeighbors;
        #}
    }

}


#Neighbor adjustment
my @Loops;
for (my $i=0; $i<$N; $i++) {
    $Loops[$i] = 0;
}
$prev = -1;
my $loopIndex=0;
my @loopNum;
for (my $i=0; $i<$N; $i++) {
    #if ($nNeighbors[$i]>0) {
    #  if ($i>0) {
    #    my $nn = $burriness[$i-1][0];
    #    $nn += 2*$nNeighbors[$i];
    #    $burriness[$i-1][0] = $nn;
    #  }
    #  if ($i<$N-1) {
    #    my $nn = $burriness[$i+1][0];
    #    $nn += 2*$nNeighbors[$i];
    #    $burriness[$i+1][0] = $nn;
    #  }
    #  my $nn = $burriness[$i][0];
    #  $nn += (2*$nNeighbors[$i]);
    #  $burriness[$i][0] = $nn;
    #}
    if ($prev>-1) {
        if ($basepair[$prev]<$prev) {#pre-caution
            printf STDERR ("error : %d %d\n",$prev, $basepair[$prev]);
            exit(1);
        }
        if ($basepair[$i]>-1) {
            if ( ($i==$prev+1) && ($basepair[$i]==$basepair[$prev]-1) ) {
                #printf STDERR ("$i continue\n");
                $prev = $i;
            }
            else {
                if ($i==$basepair[$prev]) {# a hairpin
                    #printf STDERR ("a hairpin: %d %d\n",$prev, $i);
                    $prev = -1;
                }
                else {
                    if ($basepair[$i]<$basepair[$prev] && $basepair[$i]>$i) {
                        if (abs($i-$prev)==1 || abs($basepair[$i]-$basepair[$prev])==1) {
                            #printf STDERR ("an buldge loop: %d\t%d\n", $prev, $i);
                        }
                        else {
                            #printf STDERR ("an internal loop: %d\t%d\n", $prev, $i);
                            $loopIndex++;
                            my $j=$prev+1;
                            my $nn=0;
                            for (; $j<$i; $j++) {
                                #printf STDERR ("%d\n",$j+1);
                                $Loops[$j] = $loopIndex;
                                $nn++;
                            }
                            $j = $basepair[$i]+1;
                            while ($j<$basepair[$prev]) {
                                if ($basepair[$j]>$j && $basepair[$j]<$basepair[$prev]) {
                                    $j=$basepair[$j];
                                }
                                #printf STDERR ("%d\n",$j+1);
                                $Loops[$j] = $loopIndex;
                                $nn++;
                                $j++;
                            }
                            #printf STDERR ("%d\n", $nn);
                            $loopNum[$loopIndex-1] = $nn;
                        }
                        $prev = $i;
                    }
                    else {
                        if ($basepair[$i]>$basepair[$prev]) {
                            #printf STDERR ("an possible psuedoknot: %d\t%d -- %d\t%d\n",$prev,$basepair[$prev],$i,$basepair[$i]);
                            $prev = $i;
                        }
                    }
                }
            }
        }
    }
    else {
        if ($basepair[$i]>$i) {
            #printf STDERR ("$i Start Helix\n");
            $prev = $i;
        }
    }
}

$prev=-1;
for (my $i=0; $i<$N; $i++) {
    if ($Loops[$i]>0){
        if ($prev<0) {
            $prev = $i;
        }
        #printf STDERR ("%d\n",$i+1);
    }
    else {
        if ($prev>0) {
            #printf STDERR ("%d %d %d %d\n",$prev+1, $i, $loopNum[$Loops[$prev]-1], $loopNum[$Loops[$prev]-1]-($i-$prev));
            my $max = $loopNum[$Loops[$prev]-1]-($i-$prev);
            if ($max>4) {
                $max = 4;
            }
            #printf STDERR $max . "\n";
            for (my $j=$prev; $j<$i; $j++) {
                my $adjust = $j-$prev+1;
                if ($adjust>$max) {
                    $adjust = $max;
                }
                #printf STDERR $adjust . "\n";
                #printf STDERR ("ADJUST %d %d %d\n",$Loops[$prev], $j+1, $adjust);
                if ($burriness[$j][0]>0 && $adjust>0) {
                    $burriness[$j][0] += $adjust;
                }
            }
        }
        $prev = -1;
    }
}

for (my $i=0; $i<$N; $i++) {
    #printf STDERR ("%d %d\n", $i+1, $Loops[$i]);
    printf("A%d\t%f\t%f\t%f\n",  $i+1,
        $burriness[$i][0],
        $burriness[$i][1],
        $burriness[$i][2]);
}

