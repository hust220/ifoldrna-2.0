#!/usr/bin/perl

if (@ARGV<1) {
  printf("usage: HRP-ana.pl HRPFile\n");
  exit(1);
}

my @HRP;
my $i=0;
open(IN,"<$ARGV[0]");
while (<IN>) {
  chomp();
  my @tmp_array = split(" ",$_);
  $HRP[$i] = $tmp_array[1];
  $i++;
}
close(IN);

my @HRP_sorted = sort {$a <=> $b} @HRP;
for (my $i=0; $i<@HRP_sorted; $i++) {
  #printf("%d %f\n",$i+1, $HRP_sorted[$i]);
}

my $nLEN = @HRP;

my $n_15=0;
my $n_2=0;
my $n_25=0;
my $n_3=0;
for (my $i=0; $i<$nLEN; $i++) {
  if ($HRP_sorted[$i]<=0.15) {$n_1++; }
  if ($HRP_sorted[$i]<=0.2) {$n_2++; }
  if ($HRP_sorted[$i]<=0.25) {$n_25++; }  
  if ($HRP_sorted[$i]<=0.3) {$n_3++; }
}

#printf("%f %f %f %f\n", $n_1/$nLEN, $n_2/$nLEN, $n_25/$nLEN, $n_3/$nLEN);
printf("The f25 value: %f\n", $n_25/$nLEN);
