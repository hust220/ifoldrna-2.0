#!/usr/bin/perl


if (@ARGV<5) {
  printf("usage: analyze.pl pdb seq aveReactivity length nREPLICA CLUSTERS\n");
  exit(1);
}

my $len = $ARGV[3];

#get the path
my $PATH;
{
  my @tmp_array = split(/analyze.pl/, $0);
  $PATH = $tmp_array[0];
}

$getRMSD = $PATH."movie2rmsd.linux";
$getCC   = $PATH."movie2NCcorr.linux";
$merge   = $PATH."merge_col.pl";
$sparse  = $PATH."sparse-echo-1.pl";
$getENS  = $PATH."ensemble.pl";
$mov2pdb = $PATH."nseq_movie2pdb.linux";
$getPDB  = $PATH."getPDB.sh";
$getCLUS = $PATH."getCLUSTER.sh";
$anaCLUS = $PATH."analyze-oc.pl";
#print $getRMSD ."\n";
#print $getCC . "\n";
#exit(1);


$nENS1 = $ARGV[4]-1;
#
#generate RMSD file
#

$com = "for i in `seq -w 0 $nENS1`; do $getRMSD p\*\$\{i\}.\*.mov $ARGV[0] > p\$\{i\}.rmsd; done";
print $com . "\n";
system($com);

#
#generate CC file
#

$com = "for i in `seq -w 0 $nENS1`; do $getCC p\*\$\{i\}.\*.mov  $ARGV[1] $ARGV[2] > p\$\{i\}.cc; done";
print $com . "\n";
system($com);

#
#merge_col
#

$com = "for i in `seq -w 0 $nENS1`; do grep -v \"#\" p\*\$\{i\}.\*.echo > p\$\{i\}.ener;  $merge p\$\{i\}.rmsd p\$\{i\}.ener; $merge p\$\{i\}.cc p\$\{i\}.ener; $sparse  p\$\{i\}.ener 10 > p\$\{i\}.ener.1; done";
print $com . "\n";
system($com);

#
#generate the movies
#

$com = "$getPDB $ARGV[1] $mov2pdb";
print $com . "\n";
system($com);

#
#perform clustering
#
$com = "$getCLUS $PATH";
print $com . "\n";
system($com);

#cutoff
#if ($len > 0) {
#    my $a_w = 3.6*1.414;
#    my $b_w = 11.2*1.414;
#    my $ave_rmsd = $a_w*exp(0.41*log($ARGV[3]))-$b_w;
#    my $offset = $ave_rmsd/4.0;
#    if($offset<4){ $offset=4.0;}
#    $cutoff_rmsd = $ave_rmsd - $offset;
#}
#else {
#    $cutoff_rmsd = -$len;
#}

$cutoff_rmsd = 8.0;

open(OUT,">$ARGV[5]");
#printf OUT ("#LENGTH: %d\n", $ARGV[3]);
#printf OUT ("#AVERAGE RMSD BY RANDOM: %f\n", $ave_rmsd);
printf OUT ("#CUTOFF RMSD USED: %f\n", $cutoff_rmsd);
close(OUT);

#$com = "$anaCLUS OC.input OC.output DIST CUTOFF $cutoff_rmsd | grep \"Cen\" | sort -r -n -k 5 >> $ARGV[5]";
$com = "$anaCLUS OC.input OC.output DIST CUTOFF $cutoff_rmsd > cluster.out";
print $com . "\n";
system($com);

$com = "$getENS cluster.out | sort -rn -k 4 >> $ARGV[5]";
print $com . "\n";
system($com);
