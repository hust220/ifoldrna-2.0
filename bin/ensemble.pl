#!/usr/bin/perl

if (@ARGV<1) {
  printf ("usage: ensemble.pl clusters [output]\n");
  exit(1);
}

open(IN,"<$ARGV[0]");
my $append = 0;
if (@ARGV>1) {
  open(OUT,">>$ARGV[1]");
  $append = 1;
}

my @clusters;
my $ncluster=0;

my @tmp_array;
my $tmp_count=0;
while (<IN>) {
  chomp();
  if (substr($_,0,8) eq "Cluster:") {
    $tmp_count=0;
  }
  else {
    if (substr($_,0,8) eq "Centroid") {
      my @dummy_array = split(" ", $_);
      my $cen = $dummy_array[2];
      #summerize the corresponding cluster
      my $aveC=0;
      my $stdC=0;
      my $aveR=0;
      my $stdR=0;
      my $aveE=0;
      my $stdE=0;
      for (my $i=0; $i<$tmp_count; $i++) {
	$aveE +=  $tmp_array[$i][1];
	$stdE +=  $tmp_array[$i][1]*$tmp_array[$i][1];
	
	$aveC +=  $tmp_array[$i][2];
	$stdC +=  $tmp_array[$i][2]*$tmp_array[$i][2];
	
	$aveR +=  $tmp_array[$i][3];
	$stdR +=  $tmp_array[$i][3]*$tmp_array[$i][3];
      }
      $aveE /= $tmp_count;
      $stdE /= $tmp_count;
      $stdE = sqrt($stdE-$aveE*$aveE);
      $aveC /= $tmp_count;
      $stdC /= $tmp_count;
      $stdC = sqrt($stdC - $aveC*$aveC);
      $aveR /= $tmp_count;
      $stdR /= $tmp_count;
      $stdR = sqrt($stdR - $aveR*$aveR);
      $ncluster++;
      my $minE = 1000000;
      my $iMINE = 0;
      for (my $i=0; $i<$tmp_count; $i++) {
	if ($tmp_array[$i][1]<$minE && $tmp_array[$i][2]<($aveC-$stdC)) {
	  $minE = $tmp_array[$i][1];
	  $iMINE = $i;
	}
	#my $z = ($tmp_array[$i][1]-$aveE)/$stdE + ($tmp_array[$i][2]-$aveC)/$stdC;
	##if ($z  < $minE && $tmp_array[$i][1]< $aveE && $tmp_array[$i][2]<$aveC) {
	#if ($z  < $minE ) {
	#  $minE = $z;
	#  $iMINE = $i;
	#}
      }
      if ($append) {
	printf OUT ("Cluster:%3d Size:%4d Ener:%8.3f+/-%.3lf Coef:%8.3f+/-%.3lf RMSD:%8.4f+/-%.3lf Lowest_E_Structure: %s\t Centroid: %s\n",$ncluster, $tmp_count, $aveE, $stdE, $aveC, $stdC, $aveR, $stdR, $tmp_array[$iMINE][0], $cen);
      }
      else {
	printf ("Cluster:%3d Size:%4d Ener:%8.3f+/-%.3lf Coef:%8.3f+/-%.3lf RMSD:%8.4f+/-%.3lf Lowest_E_Structure: %s\t Centroid: %s\n",$ncluster, $tmp_count, $aveE, $stdE, $aveC, $stdC, $aveR, $stdR, $tmp_array[$iMINE][0], $cen);
      }

    }
    else {
      my @dummy_array = split(" ",$_);
      my $name = $dummy_array[1];
      @dummy_array = split(/\.E/,$dummy_array[1]);
      @dummy_array = split(/\.C/,$dummy_array[1]);
      my $E = $dummy_array[0];
      @dummy_array = split(/\.R/,$dummy_array[1]);
      my $C = $dummy_array[0];
      @dummy_array = split(/\.p/,$dummy_array[1]);
      my $R = $dummy_array[0];
      #printf("E: %s Corr. Coef.: %s RMSD: %s \n",$E, $C, $R);
      $tmp_array[$tmp_count][0] = $name;
      $tmp_array[$tmp_count][1] = $E;
      $tmp_array[$tmp_count][2] = $C;
      $tmp_array[$tmp_count][3] = $R;
      $tmp_count++;
    }
  }
}
close(IN);
