#!/usr/bin/perl

if (@ARGV<5) {
  printf ("usage: prepare.pl init_pdb3 reactivity basepair boxSize outTXT\n");
  exit(1);
}

#get the path
my $PATH;
{
  my @tmp_array = split(/prepare\.pl/, $0);
  $PATH = $tmp_array[0];
}

my $PDB2TXT     = $PATH."pdb3_const2txt.linux";
my $GEN_BUR     = $PATH."assignACCESS-new.pl";
my $REACT2CONST = $PATH."react2Const-new.pl";

#print $PDB2TXT ."\n";
#print $GEN_BUR ."\n";
#print $REACT2CONST ."\n";
#exit(1);

my $init_pdb = $ARGV[0];
my $react    = $ARGV[1];
my $bp       = $ARGV[2];
my $BOX      = $ARGV[3];

my $length  = `grep " O " $init_pdb | wc -l | awk '{print $1}'`;
chomp($length);
#print $length;

print "generate the BURRINESS data.\n";
my $burriness = "burriness\.data";
$command = "$GEN_BUR $react $bp $length > $burriness";
print $command."\n";
system($command);

print "generate the pre.txt.\n";
my $pre_txt = "\.pre\.txt";
$command = "$PDB2TXT $init_pdb $bp $BOX $burriness > $pre_txt";
print $command."\n";
system($command);

print "geneate the REACT constraint.\n";
$command = "$REACT2CONST $react $bp $length $pre_txt $ARGV[4]";
print $command."\n";
system($command);
