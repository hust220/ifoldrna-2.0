#!/usr/bin/perl

if (@ARGV<3) {
  printf("usage: assignACCESS.pl reactivityFile basePairs rnaLength [inputTXT outTXT]\n");
  printf(" !!!It is ADVISED to use the raw, normalized reactivity data.!!!\n");
  printf(" reactivity format: two columns(index reactivity)\n");
  exit(1);
}

my $N = $ARGV[2];
$MAX = -1000000;
$maxN = 11.0;
$minN = 1.50;

$minE = -0.250;
$midE = -0.150;
$maxE = -0.100;

my @access;

my @data;
for (my $i=0; $i<$N+1; $i++) {
  $data[$i] = -999;
}


open(IN,"<$ARGV[0]");
my $max_num = 0;
while (<IN>) {
  chomp();
  my @tmp = split(" ",$_);
  my $index = $tmp[0]-1;
  my $react = $tmp[1];
  if ($index<0 || $index>=$N) {
    printf("error: out of range\n");
    exit(1);
  }
  $access[$index] = $react;
  $data[$index] = $react;
  if ($react>$MAX) {
    $MAX = $react;
  }
  if ($index>$max_num) {
    $max_num = $index;
  }
}
close(IN);
$max_num++;
if ($max_num>$N) {
  $max_num = $N;
}

for (my $i=0; $i<$N; $i++) {
  if ($data[$i]==-999 && ($i>0 && $data[$i-1]>-999) && ($i+1<$N && $data[$i+1]>-999)) {
    $data[$i] = ($data[$i-1]+$data[$i+1])/2;
  }
}

my @JUNK;
for (my $i=0; $i<$N;$i++) {
  if ($data[$i]>-999) {
    my $prev = $data[$i];
    if ($i>0 && $data[$i-1]>-999) {
      $prev = $data[$i-1];
    }
    my $next = $data[$i];
    if ($i+1<$N && $data[$i+1]>-999) {
      $next = $data[$i+1];
    }
    $JUNK[$i]=($next+$data[$i]+$prev)/3;
  }
}
for (my $i=0; $i<$N;$i++) {
  if ($data[$i]>-999) {
    $data[$i] = $JUNK[$i];
    #print $i+1 . " " . $data[$i] . "\n";
  }
}


open(IN, "<$ARGV[1]");
my @basepair;
for (my $i=0; $i<$N; $i++) {
  $basepair[$i] = -100;
}
while (<IN>) {
  chomp();
  my @tmp = split(" ",$_);
  my $i = substr($tmp[0],1);
  my $j = substr($tmp[1],1);
  #printf("%d %d\n",$i,$j);
  $basepair[$i-1] = $j-1;
  $basepair[$j-1] = $i-1;
}
close(IN);

my @tmp_access;
my @tmp_data;
my $tmp_idx=0;
my $tmp_didx=0;
for (my $i=0; $i<$N; $i++) {
  if (defined($access[$i])) {
    $tmp_access[$tmp_idx] = $access[$i];
    $tmp_idx++;
    $tmp_data[$tmp_didx] = $data[$i];
    $tmp_didx++;
  }
  else {
    $access[$i] = $MAX+1000;
    if ($data[$i]>-999) {
      $tmp_data[$tmp_didx] = $data[$i];
      $tmp_didx++;
    }    
  }
}

my @sorted_access = sort {$a <=> $b} @tmp_access;
my @sorted_data = sort {$a <=> $b} @tmp_data;

for (my $i=0; $i<$N; $i++) {
  if ($data[$i]<=-999) {
    $data[$i] = $MAX+1000;
  }
}

#print @sorted_access;
my $nBUR = int(0.10*$tmp_idx);
my $nEXP = int(0.30*$tmp_idx);
my $min_access = $sorted_access[$nBUR];
my $max_access = $sorted_access[$tmp_idx - $nEXP];

my @burriness;

my $putative_BUR  =  $sorted_access[int($tmp_idx*0.25)];
my $putative_hBUR  = ($sorted_access[int($tmp_idx*0.30)]+$sorted_access[int($tmp_idx*0.50)])/2;
my $putative_INTER = $sorted_access[int($tmp_idx*0.80)];
my $prev = -1;
my $prev_core = -8;
my @core_res;
my $ncore=0;

#printf("%f %f\n", $putative_hBUR, $putative_BUR);
#create the tentative bases;
for (my $i=0; $i<$N; $i++) {
  if ($access[$i]<=$putative_hBUR) {
    $CG_access[$i] = -1;
  }
  else {
    $CG_access[$i] = 0;
    if ($access[$i]>$putative_INTER) {
      $CG_access[$i] = 1;
    }
    if ($access[$i]>$MAX) {
      $CG_access[$i] = 2;
    }
  }
}
$prev=-1;
for (my $i=0; $i<$N; $i++) {
  if ($CG_access[$i]<0) {
    if ($prev<0) {
      $prev = $i;
    }
  }
  else {
    if ($prev>=0) {
      my $diff = $prev-$i;
      my $has_low = 0;
      for (my $k=$prev; $k<$i; $k++) {
	if ($access[$k]<=$putative_BUR) {
	  $has_low = 1;
	}
      }
      if ($has_low) {
	for (my $j=$prev; $j<$i; $j++) {
	  $CG_access[$j] = $diff;
	}
      }
      $prev = -1;
    }
  }
}
for (my $i=1; $i<$N-1; $i++) {
  if (($CG_access[$i]==0 && ($CG_access[$i-1]<-1 || $CG_access[$i+1]<-1))) {
    $CG_access[$i] = -1;
    #printf STDERR ("FIX: %d %d %d\n",$i+1,$CG_access[$i-1], $CG_access[$i+1]);
  }
  if ((($CG_access[$i]==0) && $CG_access[$i-1]<0 && $CG_access[$i+1]<0)){
    my $sum = $CG_access[$i-1]+$CG_access[$i+1];
    if ($sum<=-3) {
      $CG_access[$i] = $sum;
    }
  }
  if($CG_access[$i]>1 && (($access[$i-1]<$putative_BUR || $access[$i+1]<$putative_BUR) && ($CG_access[$i-1]<=0 && $CG_access[$i+1]<=0)) ){
    $CG_access[$i] = -1;
    #printf STDERR ("FIX: %d %d %d\n",$i+1,$CG_access[$i-1], $CG_access[$i+1]);
  }
}
$prev=-1;
for (my $i=0; $i<$N; $i++) {
  if ($CG_access[$i]<0) {
    if ($prev<0) {
      $prev = $i;
    }
  }
  else {
    if ($prev>=0) {
      my $diff = $prev-$i;
      if ($diff <= -3) {
	for (my $j=$prev; $j<$i; $j++) {
	  $CG_access[$j] = $diff;
	}
      }
      else {
	for (my $j=$prev; $j<$i; $j++) {
	  $CG_access[$j] = 0;
	}
      }
      $prev = -1;
    }
  }
}

#for (my $i=0; $i<$N; $i++){
#  printf("%d \t %d \t %f\n",$i+1, $CG_access[$i], $access[$i]);
#}
#exit(1);

$prev = -1;
for(my $i=5; $i<$max_num;$i++) {
  #if ($access[$i]<$putative_hBUR) {
  if ($CG_access[$i]<0 && $i<$max_num-1) {
    if ($prev<0) {
      $prev = $i;
    }
  }
  else {
    if ($prev>=0) {
      if (($i-$prev)>=3) {
	my $diff=$i-$prev;
	my $nn = int($diff/10);
	if ($nn==0) {
	  $nn=1;
	}
	my $step = $diff/$nn;
        #printf("-- %d,  %d\n",$prev+1, $i);
	for (my $istep=0; $istep<$nn; $istep++) {
	  my $min = $MAX+100;
	  my $min_idx;
	  my $start = $prev+$istep*$step+1;
	  if ($istep>0) {
	    $start-=1;
	  }
	  my $end = $prev+($istep+1)*$step-1;
	  if ($istep<$nn-1) {
	    $end+=1;
	  }
	  for (my $j=$start; $j<$end; $j++) {
	    if ($access[$j]<$min) {
	      $min = $access[$j];
	      $min_idx = $j;
	    }
	  }
	  #printf("%d %d : ", $min_idx+1, $CG_access[$min_idx]);
	  if ($min<=$putative_BUR ||
	      $access[$min_idx-1]<=$putative_BUR ||
	      $access[$min_idx+1]<=$putative_BUR ) {
	    #printf("%d %d\n", $min_idx+1, $CG_access[$min_idx]);
	    $core_res[$ncore] = $min_idx;
	    $ncore++;
	  }
	  #else {
	  #  printf("\n");
	  #}
	}
      }
      $prev = -1;
    }
  }
}

#check whether two cores are close to each other
my @nNeighbors;
for (my $i=0; $i<$N; $i++) {
  $nNeighbors[$i] = 0;
}

for (my $i=0; $i<@core_res; $i++) {
  my $icore = $core_res[$i];
  for (my $j=$i+1; $j<@core_res; $j++) {
    my $isNeighbor = 0;
    my $jcore = $core_res[$j];
    if ($basepair[$icore]>=0) {
      if ($basepair[$jcore]>=0) {
	if (($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 )||
	    ($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )||
	    (abs($basepair[$icore]-$basepair[$jcore])<=2)  ) {
	  #printf("1-neighbors: %d %d\n",$icore+1,$jcore+1);
	  $isNeighbor=1;
	  #count these that are !EXcluded
	  if (!($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 ) &&
	      !($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )) {
	    $nNeighbors[$icore]++;
	    $nNeighbors[$jcore]++;
	  }
	}
      }
      else {
	if (($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 ) ||
	    ($basepir[$jcore-1]>=0 && abs($basepair[$jcore-1]-$basepair[$icore])<=1) ||
	    ($basepir[$jcore+1]>=0 && abs($basepair[$jcore+1]-$basepair[$icore])<=1) ){
	  #printf("2-neighbors: %d %d\n",$icore+1, $jcore+1);
	  $isNeighbor=1;
	  #count these that are !EXcluded
	  if (!($jcore>=$basepair[$icore]-1 && $jcore<=$basepair[$icore]+3 )) {
	    $nNeighbors[$icore]++;
	    $nNeighbors[$jcore]++;
	  }
	}
      }
    }
    else {
      if ($basepair[$jcore]>=0) {
	if (($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 ) ||
	    ($basepir[$icore-1]>=0 && abs($basepair[$icore-1]-$basepair[$jcore])<=1) ||
	    ($basepir[$icore+1]>=0 && abs($basepair[$icore+1]-$basepair[$jcore])<=1) ) {
	  #printf("3-neighbors: %d %d\n", $icore+1, $jcore+1);
	  $isNeighbor=1;
	  #count these that are !EXcluded
	  if (!($icore>=$basepair[$jcore]-1 && $icore<=$basepair[$jcore]+3 )) {
	    $nNeighbors[$icore]++;
	    $nNeighbors[$jcore]++;
	  }
	}
      }
      else {
	for (my $k=$icore-2; $k<=$icore+2; $k++) {
	  if ($basepair[$k]>=0){
	    my $tmp=($k-$icore)*($jcore-$basepair[$k]);
	    if ($tmp>=0 && $tmp<=2) {
	      $isNeighbor=1;
	      #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
	    }
	  }
	}
	for (my $k=$jcore-2; $k<=$jcore+2; $k++) {
	  if ($basepair[$k]>=0){
	    my $tmp=($k-$jcore)*($icore-$basepair[$k]);
	    if ($tmp>=0 && $tmp<=2) {
	      $isNeighbor=1;
	      #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
	    }
	  }
	}
	if ($isNeighbor) {
	  #printf("4-neighbors: %d %d\n",$icore+1, $jcore+1);
	  $nNeighbors[$icore]++;
	  $nNeighbors[$jcore]++;
	}
      }
    }
  }
}

for (my $i=0; $i<$N; $i++) {
  if ($nNeighbors[$i]>0) {
    #printf("%d has %d neightbors\n",$i+1, $nNeighbors[$i]);
  }
}

for (my $i=0; $i<@core_res; $i++) {
  if ($core_res[$i]>=0) {
    printf("core resi: %d\n", $core_res[$i]+1);
  }
  else {
    printf("shared core resi: %d\n", -$core_res[$i]+1);
  }
}

#exit(1);

#my $putative_EXP =  $sorted_access[int($tmp_idx*0.80)];
#my $putative_hEXP = $sorted_access[int($tmp_idx*0.60)];
my $putative_EXP =  $sorted_data[int($tmp_didx*0.80)];
my $putative_hEXP = $sorted_data[int($tmp_didx*0.60)];
my $prev_EXP = -10;
$prev = -1;

#printf $putative_EXP . " " . $putative_hEXP . " $tmp_didx @sorted_data \n";
my $JUNK = @sorted_data;
printf("%f %f %d %d\n",$putative_EXP, $putative_hEXP, $tmp_didx, $JUNK );

for (my $i=1; $i<$N-1; $i++) {
  #if ($access[$i]>$putative_hEXP && $access[$i]<=$MAX) {
  if ($data[$i]>$putative_hEXP && $data[$i]<=$MAX) {
    if ($prev<0) {
      $prev = $i;
    }
  }
  else {
    if ($prev>=0) {
      if (($i-$prev)>=3) {
	my $max = -100000;
	my$max_idx;
	#printf("%d %d\n",$prev, $i);
	for (my $j=$prev; $j<$i; $j++) {
	  #if ($access[$j]>$max) {
	  if ($data[$j]>$max) {
	    #$max = $access[$j];
	    $max = $data[$j];
	    $max_idx = $j;
	  }
	}
	if ($max>$putative_EXP && $max_idx>($prev_EXP+9)) {
	  printf("EXP: %d\n",$max_idx+1);
	  $prev_EXP = $max_idx;
	}
      }
      $prev=-1;
    }
  }
}

my $input_txt = 0;
my $the_line=0;
if (@ARGV>=5) {
  $input_txt = 1;
  open(IN,"<$ARGV[3]");
  open(OUT,">$ARGV[4]");
  @INPUT = <IN>;
  close(IN);
  my $reach_const=0;
  for (my $i=0; $i<@INPUT; $i++) {
    if (! $reach_const){
      if ($INPUT[$i] eq "N.CONSTRAINTS\n") {
	$reach_const=1;
      }
      printf OUT ("%s",$INPUT[$i]);
      $the_line++;
    }
  }
}

my $Rg = pow($N, 0.41)*3.8;
#printf("Rg: %f\n",$Rg);
my $min_d = 3.0;
my $mid_d = 3.0*$Rg;
my $nStep = 5;
my $step = $Rg/$nStep;
my $kT = 0.05;
my $pot = sprintf("%.4f %.4f %.4f",$min_d, $mid_d, -$kT);
my $d = $mid_d;
for (my $i=1; $i<2*$nStep; $i++) {
  $d += $step;
  my $e = -(2*$i+1)*$kT;
  $pot .= sprintf(" %.4f %.4f", $d, $e);
}
$d = 1000;
$pot .= sprintf(" %.4f %.4f %.4f", $d-0.1, 4*$kT*($nStep*$nStep), $d);
#print $ncore;
for (my $i=0; $i<$ncore; $i++) {
  my $ii = $core_res[$i];
  if($ii<0){ $ii*=-1;}
  my $sugar = 3*$ii+1;
  for (my $j=$i+1; $j<$ncore; $j++) {
    my $jj = $core_res[$j];
    if($jj<0){ $jj*=-1;}
    my $nsugar = 3*$jj+1;
    printf("%5d %5d %s\n",$sugar, $nsugar, $pot);
    if ($input_txt) {
      printf OUT ("%5d %5d %s\n",$sugar, $nsugar, $pot);
    }
  }
}

if ($input_txt) {
  for (my $i=$the_line; $i<@INPUT; $i++) {
    printf OUT ("%s",$INPUT[$i]);
  }
  close(OUT);
}

sub pow(){
  my $a=$_[0];
  my $b=$_[1];
  return exp(log($a)*$b);
}
