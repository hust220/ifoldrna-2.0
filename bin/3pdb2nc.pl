#!/usr/bin/perl

if (@ARGV<2) {
  printf("usage: 3pdb2nc.pl cutoff pdb3 [...]\n");
  exit(1);
}
my $D2 = $ARGV[0]*$ARGV[0];
my @chains;
my $nchain;
my @nres;
&read($ARGV[1]);

#compute the contacts
#At the moment, only compute the first chain
my @aveNC;
my @stdNC;
my @nc;
for (my $i=0; $i<$nres[0]; $i++) {
  $nc[$i] = 0;
  $aveNC[$i]=0;
  $stdNC[$i]=0;
}
for (my $ir=0; $ir<$nres[0]; $ir++) {
  for (my $jr=$ir+3; $jr<$nres[0];$jr++) {
    my $r = ($chains[0][$ir][0]-$chains[0][$jr][0]);
    my $d=$r*$r;
    $r = ($chains[0][$ir][1]-$chains[0][$jr][1]);
    $d += $r*$r;
    $r = ($chains[0][$ir][2]-$chains[0][$jr][2]);
    $d += $r*$r;
    if ($d < $D2 ) {
      #printf("$ir $jr $d $D2\n");
      $nc[$ir]++;
      $nc[$jr]++;
    }
  }
}
for (my $i=0; $i<$nres[0];$i++) {
  $aveNC[$i] += $nc[$i];
  $stdNC[$i] += $nc[$i]*$nc[$i];
}

if (@ARGV>2) {
  for (my $iarg=2; $iarg<@ARGV; $iarg++) {
    &read($ARGV[$iarg]);
    for (my $i=0; $i<$nres[0]; $i++) {
      $nc[$i] = 0;
    }
    for (my $ir=0; $ir<$nres[0]; $ir++) {
      for (my $jr=$ir+3; $jr<$nres[0];$jr++) {
	my $r = ($chains[0][$ir][0]-$chains[0][$jr][0]);
	my $d=$r*$r;
	$r = ($chains[0][$ir][1]-$chains[0][$jr][1]);
	$d += $r*$r;
	$r = ($chains[0][$ir][2]-$chains[0][$jr][2]);
	$d += $r*$r;
	if ($d < $D2 ) {
	  #printf("$ir $jr $d $D2\n");
	  $nc[$ir]++;
	  $nc[$jr]++;
	}
      }
    }
    for (my $i=0; $i<$nres[0];$i++) {
      $aveNC[$i] += $nc[$i];
      $stdNC[$i] += $nc[$i]*$nc[$i];
    }
  }
  my $npdb = @ARGV-1;
  for (my $i=0; $i<$nres[0];$i++) {
    $aveNC[$i] /= $npdb;
    $stdNC[$i] /= $npdb;
    $stdNC[$i] = sqrt($stdNC[$i]-$aveNC[$i]*$aveNC[$i]);
    printf("%d %f %f\n",$i+1, $aveNC[$i], $stdNC[$i]);
  }
}

#testing
#printf("chains: %d\n", $nchain);
#for (my $ic=0; $ic<$nchain; $ic++) {
#  printf("chain: %d residues: %d\n", $ic+1, $nres[$ic]);
#  for (my $ires=0; $ires<$nres[$ic]; $ires++) {
#    printf("x: %f y: %f z: %f\n",
#	   $chains[$ic][$ires][0],
#	   $chains[$ic][$ires][1],
#	   $chains[$ic][$ires][2]);
#  }
#}
#read PDB
#argument: pdb
sub read(){
  my $pdb = $_[0];
  my $ichain = 0;
  my $ires=0;
  open(IN,"<$pdb");
  while (<IN>) {
    if (substr($_,0,3) eq "TER") {
      $nres[$ichain] = $ires;
      $ichain++;
      $ires=0;
    }
    else {
      if ((substr($_,0,6) eq "ATOM  " ) && (substr($_,12,3) eq " C ")) {#only take suger bead
	$x = substr($_,30,8);
	$y = substr($_,38,8);
	$z = substr($_,46,8);
	$chains[$ichain][$ires][0] = $x;
	$chains[$ichain][$ires][1] = $y;
	$chains[$ichain][$ires][2] = $z;
	$ires++;
      }
    }
  }
  if ($ires>0){#last chain
    $nres[$ichain] = $ires;
    $ichain++;
  }
  close(IN);
  $nchain = $ichain;
}
