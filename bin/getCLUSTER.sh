#!/bin/sh

ls -1 p*pdb > list
$1/genOCInput.pl list  > OC.input
head -n 1 OC.input > OC.1
awk '{a++; print a}' list >> OC.1
wc -l OC.input | awk '{printf("tail -n %d OC.input >> OC.1\n",$1-b)}' b=`wc -l list|awk '{print $1+1}'` | sh
$1/oc dis means  < OC.1 > OC.output
#/ifs1/scr/fding/Hydroxyl/cluster/oc dis complete  < OC.1 > OC.output
