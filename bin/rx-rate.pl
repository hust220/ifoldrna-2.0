#!/usr/bin/perl

if (@ARGV<1) {
  printf("usage: rx-rate.pl rx-temp-out\n");
  exit(1);
}

open(IN,"$ARGV[0]");
$iline=0;
while (<IN>) {
  chomp();
  if (substr($_,0,1) eq '#') {
    #skip
    #printf("$_\n");
  }
  else {
    @line = split(" ", $_);
    if ($line[0] == 0) {
      $ntemp = @line -1 ;
      #printf("first line $ntemp\n");
      #sort the temp
      for (my $i=0;$i<$ntemp;$i++) {
        $prev[$i] = $line[$i+1];
        $exchnge[$i] = 0;
      }
      @sorted = sort(@prev);
      #print $sorted[2];
    }
    else {
      if (defined($ntemp)) {
        for (my $i=0;$i<$ntemp;$i++) {
          $curr[$i] = $line[$i+1];
        }
        for (my $i=0; $i<$ntemp; $i++) {
          if ($curr[$i]<$prev[$i]) {
            #exchanged
            my $index = value2index($curr[$i]);
            $exchange[$index] += 1;
          }
          $prev[$i] = $curr[$i];
        }
        #printf("hi\n");
        $iline++;
      }
    }
  }
}

for (my $i=0; $i<$ntemp-1; $i++) {
  printf("%f <-> %f: %f\n",$sorted[$i],$sorted[$i+1],$exchange[$i]/($iline/2.0));
}

sub value2index{
  my $t = $_[0];
  for (my $i=0; $i<$ntemp; $i++) {
    if ($t == $sorted[$i]) {
      return $i;
    }
  }
  printf("error temperature! ABORT $t\n");
  exit(1);
}
