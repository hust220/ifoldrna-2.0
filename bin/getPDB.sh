#!/bin/sh

if [ $# -lt 1 ]; then
    echo "usage: command seqFile movie2pdb_exec"
    exit 1;
fi


#for i in `ls p00?.ener`; do awk '{print $3+$6+$8" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 3 | head -n 1000 | sort -n -k 1 | head -n 100 | awk '{printf("/ifs1/home/fding/scratch/Hydroxyl/bin/nseq_movie2pdb.linux %s.*.mov %s %s.N%d.E%.3f.C%.3f.R%.3f.pdb %d 1 1\n", substr($5,1,4), S, substr($5,1,4), int($4/10), $1,$3,$2, int($4/10)); }' S=$1 | sh
#for i in `ls p00?.ener`; do awk '{print $3+$6+$8" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 1 | head -n 1000 | sort -n -k 3 | head -n 100 | awk '{printf("/ifs1/home/fding/scratch/Hydroxyl/bin/nseq_movie2pdb.linux %s.*.mov %s %s.N%d.E%.3f.C%.3f.R%.3f.pdb %d 1 1\n", substr($5,1,4), S, substr($5,1,4), int($4/10), $1,$3,$2, int($4/10)); }' S=$1 | sh
#for i in `ls p00?.ener.1`; do awk '{print $13" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 3 | head -n 500 | sort -n -k 1 | head -n 100 | awk '{printf("/ifs1/home/fding/scratch/Hydroxyl/bin/nseq_movie2pdb.linux %s.*.mov %s %s.N%d.E%.3f.C%.3f.R%.3f.pdb %d 1 1\n", substr($5,1,4), S, substr($5,1,4), int($4/10), $1,$3,$2, int($4/10)); }' S=$1 | sh

#for i in `ls p00?.ener`; do awk '{print $3+$6+$8" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 1 | head -n 1000 | sort -n -k 3 | head -n 100 > j1
#for i in `ls p00?.ener`; do awk '{print $3+$6+$8" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 3 | head -n 1000 | sort -n -k 1 | head -n 100 >> j1

for i in `ls p*.ener.1`; do awk '{print $13" "$11 " " $12 " " $1 " " a}' a=$i $i ; done | grep -v "e-" | sort -n -k 1 | head -n 100 > j1
sort -n -k 1 j1  | uniq > j2
awk '{printf("%s p%03d.*.mov %s p%03d.N%d.E%.3f.C%.3f.R%.3f.pdb %d 1 1\n", E, substr($5,2,index($5,".")-2), S, substr($5,2,index($5,".")-2), int($4/10), $1,$3,$2, int($4/10)); }' S=$1 E=$2 j2 | sh
