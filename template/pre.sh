#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -l walltime=24:00:00
#PBS -A open

module load hrp

############### Step 1 First round of MD

pwd
cd ${PBS_O_WORKDIR}
pwd

N=$(wc -l seq | perl -lane 'print $F[0]-1')
echo "Number of Residue: $N"

echo "Generate the 'pre.txt' file"
nseq_cons2txt.linux seq bp.const 1000 $RANDOM>pre.txt # 1000 is the boxsize

echo "Run DMD"
dmd.linux -i relax

echo "Convert the movie to pdb file"
# >>> mov2pdbs.linux movie seq outPDB [initFrame nFrame dN]
nseq_movie2pdb.linux relax.mov seq relaxed.pdb 1000 1 1


################ Step 2 Second round of MD

echo "Run DMD"
dmd.linux -i relax-loop -L

echo "Convert movie to pdb"
nseq_movie2pdb.linux relax-L.mov seq init.pdb  100 1 1


################ Step 3 REMD

echo "Prepare for REMD"
prepare.pl init.pdb MBox.hrp bp.const 500 init1.txt

echo "Add constraints"
python add-constraints.py init1.txt constraints.txt init.txt

