#! /home/wangjian/programs/Anaconda-2.1.0/bin/python

import sys

pairs = ['(', ')', '[', ']', '{', '}', '<', '>']
#pairs = ['(', ')']
singles = ['.', ':']

chains = ['&', ' ']

def concat_ss(ss):
    new_ss = ''
    breaks = []

    i = 0
    old_c = '^'
    for c in ss:
        if (old_c in chains) and (c not in chains):
            breaks.append(i-1)
        if (c not in chains):
            new_ss += c
            i += 1
        old_c = c

    return (new_ss, breaks)

def get_chain(i, breaks):
    c = 'A'
    for n in breaks:
        if i > n:
            c = chr(ord(c) + 1)
        else:
            return c
    return c

def get_residue(i, breaks):
    r = i + 1
    for n in breaks:
        d = i - n
        if d > 0:
            r = i - n
        else:
            return r
    return r

def db2bp(ss):
    global pairs, singles
    ss, breaks = concat_ss(ss)
    l = len(ss)
    stacks = [[] for i in pairs]
    for i in range(0, l):
        if ss[i] in pairs:
            index = pairs.index(ss[i])
            if index % 2 == 0:
                stacks[index].append(i)
            else:
                a = stacks[index-1][-1]
                b = i
                print '{0}{1} {2}{3}'.format(get_chain(a, breaks), get_residue(a, breaks), get_chain(b, breaks), get_residue(b, breaks))
                stacks[index-1].pop()

f = open(sys.argv[1])
ss = f.read().strip()
f.close()
db2bp(ss)

