module load hrp

mkdir -p clusters

n=1
grep 'Centroid' clustering | perl -lane '/Centroid: (\S+)/; print $1' | while read pdb_3bd; do
#for pdb_3bd in $(grep clustering | perl -lane '/Centroid: (\S+)/; print $1'); do
    echo $pdb_3bd

#for i in $(seq 1 5); do
#    pdb_3bd=$(perl -lane 'if(/Centroid: (\S+)/){$n++;if($n=='$i'){print $1;break}}' clustering)
    if [ -f ${pdb_3bd} ]; then
        $HRP_HOME/iFoldRNA/MEDUSArecon_3BeadRNA.exe $HRP_HOME/iFoldRNA/medusa/parameter/ ${pdb_3bd} >clusters/cluster.${n}.pdb
        n=$((n+1))
    fi
done
