cd $1

if [ $(ls *.echo | wc -l) -eq 0 ] || [ $(ls *.echo | wc -l) -eq 0 ] || [ $(ls seq | wc -l) -eq 0 ] || [ $(ls bp.const | wc -l) -eq 0 ]; then
    pwd
    echo 'Not a DMD simulation directory!'
else
    tmp=.$RANDOM
    mkdir ${tmp}
    mv * $tmp
    mv $tmp/*.echo $tmp/*.mov $tmp/seq $tmp/bp.const $tmp/constraints.txt .
    rm -rf $tmp
fi

cd -
