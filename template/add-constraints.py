import sys
import re

txt_file = sys.argv[1]
cons_file = sys.argv[2]
out_file = sys.argv[3]

keys = []
cons = []

for line in open(cons_file):
    l = re.split('\s+', line.strip())
    if len(l) > 0:
        cons.append(line)
        i = int(l[0])
        j = int(l[1])
        if i > j:
            i, j = j, i
        keys.append((i, j))

f = open(out_file, 'w+')

flag = False
for line in open(txt_file):
    if line.strip() == 'O.STACKING LIST':
        for con in cons:
            f.write(con)
        flag = False

    if flag:
        l = re.split('\s+', line.strip())
        if len(l)>0:
            i = int(l[0])
            j = int(l[1])
            if i > j:
                i, j = j, i

            if (i, j) not in keys:
                f.write(line)
            else:
                print 'Duplicate:', line.strip()
    else:
        f.write(line)

    if line.strip() == 'N.CONSTRAINTS':
        flag = True

f.close()

