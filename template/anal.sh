#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -l walltime=24:00:00
#PBS -A open

module load hrp

pwd
cd ${PBS_O_WORKDIR}
pwd

N=$(wc -l seq | perl -lane 'print $F[0]-1')
echo "Number of Residue: $N"

# Prepare the average reaction file
# [Smoothing the orignal reactivity data increases the correlation to the known 3D strucutrue.]
# >>> /path/of/average-reactivity.pl reactivity length
average-reactivity.pl MBox.hrp $N >ave.hrp

# Get native pdb
$HRP_HOME/iFoldRNA/MEDUSArecon_3BeadRNA.exe $HRP_HOME/iFoldRNA/medusa/parameter/ relaxed.pdb >allatom.pdb

# Clustering
analyze.pl allatom.pdb seq ave.hrp $N 8 clustering

