import sys
import re

pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]

seq_file = sys.argv[1]
ss_file = sys.argv[2]

def get_ind(seq, chain, res):
    lens = [len(i) for i in seq]
    pre = sum([0] + lens[:chain])
    ind = pre + res
#    print chain, res, ind
    return ind

seq = []
s = ''
for line in open(seq_file):
    l = line.strip()
    if len(l) > 0:
        if l != 'TER' and l != 'END':
            s += l[0]
        else:
            seq.append(s)
            s = ''

n = sum([len(i) for i in seq])

ss = ['.' for i in range(n)]

def set_ss(ss, a, b):
    if ss[a] == '.' and ss[b] == '.':
        if a > b:
            a, b = b, a
        for pair in pairs:
            s = 0
            for i in range(a + 1, b):
                if ss[i] == pair[0]:
                    s += 1
                elif ss[i] == pair[1]:
                    s -= 1
            if s == 0:
                ss[a] = pair[0]
                ss[b] = pair[1]
                return

for line in open(ss_file):
    l = re.split('\s+', line.strip())
#    print l
    if len(l) == 2:
#        print l
        chain1 = ord(l[0][0]) - ord('A')
        res1 = int(l[0][1:]) - 1
        a = get_ind(seq, chain1, res1)
        chain2 = ord(l[1][0]) - ord('A')
        res2 = int(l[1][1:]) - 1
        b = get_ind(seq, chain2, res2)
        set_ss(ss, a, b)

ss = ''.join(ss)
seq_str = '&'.join(seq)
breaks = []
i = 0
ss_str = ''
for c in seq_str:
    if c != '&':
        ss_str += ss[i]
        i += 1
    else:
        ss_str += '&'


print seq_str
print ss_str

