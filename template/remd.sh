#!/bin/bash

#PBS -l nodes=1:ppn=8
#PBS -l walltime=48:00:00
#PBS -A open

module load hrp

pwd
cd ${PBS_O_WORKDIR}
pwd

mpirun -np 8 $HRP_HOME/bin/dmd-rx-mc.linux -i start -r start-8.rex -L

