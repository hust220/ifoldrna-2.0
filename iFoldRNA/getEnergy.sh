#!/bin/bash

E_UNAVAILABLEPARAMS=66

# Load iFoldRNA configuration:
. iFoldRNAConfig.sh

if [ -z "$1" ]
then
  echo "Syntax: `basename $0` <input-RNA-ID>"
  exit $E_NOARGS
fi

rnaid=$1

for i in `seq 0 7`; do  
  if [ -e "./p00${i}.${rnaid}.echo" ] 
  then true 
  else
    echo "Unable to find necessary input file: p00${i}.${rnaid}.echo" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi;
done

for  i in `seq 0 7`; do 
cat  p00${i}.${rnaid}.echo | grep  -v '#' | awk '{print $3 + $8}'  > p00${i}.${rnaid}.ener ; 
done;