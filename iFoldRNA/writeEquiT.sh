#!/usr/bin/env bash

taskfilename=${1}
temp=${2}
restart=${3}
echo=${4}
movie=${5}

echo "THERMOSTAT              ANDERSON
T_NEW                   ${2}
T_LIMIT                 ${2}
HEAT_X_C                10
RESTART_FILE             ${3}
RESTART_DT              100
ECHO_FILE               ${4}
ECHO_DT                 10
MOVIE_FILE              ${5}
MOVIE_DT                10
START_TIME              0
MAX_TIME                10000" > ${taskfilename}
