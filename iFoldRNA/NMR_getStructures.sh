#!/bin/bash

# prerequisites
#	clustering has been performed, results stored in 'clustering.txt'
#
# post-execution
#	all-atom structures craeted,
#	structures to be returned and their energies listed in freeEnergyStructureMap.txt

. iFoldRNAConfig.sh

# doesn't really do anything, just consistent with iFoldRNAv1
rnaid=$1
# for debugging give second argument as 'debug'
debug=$2
#debug=$1

output_map=freeEnergyStructureMap.txt
echo -e "<PDB-File-Name> <Free-Energy>" > $output_map

# for each cluster
grep 'Cluster: ' clustering.txt |
while read cluster
do
	[ "$debug" == "debug" ] && echo "Processing cluster: " $cluster
	
	# grab the centroid of the cluster
	rna3bead=$(echo $cluster | sed 's@.* Centroid: \(.*pdb\) .*@\1@g')

	[ "$debug" == "debug" ] && echo "Processing 3 bead model: " $rna3bead

	# use this for unique names
	rna_bname=$(basename $rna3bead .pdb)
	
	# name of the all atom model to be created
	all_atom=${rna_bname}_allatom.pdb
	
	[ "$debug" == "debug" ] && echo "Creating all atom model: " $all_atom

	# convert from 3 bead to all atom
	${IFOLDRNADIR}/NMR/recon_3BeadRNA.linux ${MEDUSADIR}/medusa/parameter/ $rna3bead >  $all_atom
	
	[ "$debug" == 'debug' ] && echo "Writing basepairing"

	# rewrite the basepairing in format used by complex-1.linux
	# e.g. A1 A10   -->   1 10
	#cat ${rnaid}.const | sed 's/A/1\./g' | sed 's/^/BasePair /g' | tr "\t" " " > $inConst

	inConst=${rnaid}.INconst

	#build xplr2INconst.py command
	if [ -f ${rnaid}_Hg.bp ];
	then
		./checkHRP.sh ${rnaid}_Hg.bp
		HG=" --hg ${rnaid}_Hg.bp "
	else
		HG=""
	fi

	if [ -f ${rnaid}_RevWC.bp ];
	then
		./checkHRP.sh ${rnaid}_RevWC.bp
		revWC=" --rwc ${rnaid}_RevWC.bp "
	else
		revWC=""
	fi

	if [ -f ${rnaid}_RevHg.bp ];
	then
		./checkHRP.sh ${rnaid}_RevHg.bp
		revHg=" --rhg ${rnaid}_RevHg.bp "
	else
		revHg=""
	fi

	xplrStr="${IFOLDRNADIR}/NMR/xplr2INConst.py ${rnaid}.seq ${rnaid}.const ${rnaid}_filter.mr ${HG} ${revWC} ${revHg} > ${inConst}"
	python ${xplrStr}

	[ "$debug" == "debug" ] && echo "Running complex.linux"

	# prepare paramFile, stateFile and constraintFile for dmd
	rand_seed=7
	paramDir=${MEDUSADIR}/medusa/parameter/
	dimen=300
	paramFile=${rna_bname}.cplxParam
	stateFile=${rna_bname}.cplxState
	constraintFile=${rna_bname}.cplxConst
	${IFOLDRNADIR}/NMR/complex.linux \
		-P $paramDir \
		-I $all_atom \
		-D $dimen \
		-p $paramFile \
		-s $stateFile \
		-S $rand_seed \
		-C $inConst \
		-c $constraintFile

	#[ "$debug" == "debug" ] && echo "Writing thermostat taskfile"

	# prepare thermostat task file
	#therm55_file=${rna_bname}.equi.T55
	#temp=0.55
	#restart_name=${rna_bname}.T55.dmd.restart
	#echo_name=${rna_bname}.T55.dmd.echo
	#movie_name=${rna_bname}.T55.dmd.mov
	#./writeEquiT.sh \
	#	${therm55_file} \
	#	$temp \
	#	${restart_name} \
	#	${echo_name} \
	#	${movie_name}

	[ "$debug" == "debug" ] && echo "Running dmd"

	# run DMD
	#
	# ./xDMD.linux
	# usage: ./xDMD.linux -p paramFile -s stateFile -t txtFile -i taskFile -c constraintFile
	#   paramFile -- the parameter file for DMD simulation
	#   stateFile -- the state file or restart for DMD simulation
	#   txtFile   -- the txt file
	#                ONLY ONE of txtFile or (paramFile+stateFile) is REQUIRED
	#   taskFile  -- the task file
	#   constraintFile  -- the user-defined constraint file, Optional
	#${IFOLDRNADIR}/xDMD.linux \
	#	-p $paramFile \
	#	-s $stateFile \
	#	-i $therm55_file \
	#	-c $constraintFile

	${IFOLDRNADIR}/NMR/pdmd.linux \
		-p $paramFile \
		-s $stateFile \
		-i ${IFOLDRNADIR}/HRP-1.0/doc/relax
		-c $constraintFile
		-C $inConst

	${IFOLDRNADIR}/NMR/pdmd.linux \
		-p $paramFile \
		-s dmd_restart \
		-i ${IFOLDRNADIR}/NMR/relax-1.task
		-c $constraintFile
		-C $inConst

	${IFOLDRNADIR}/NMR/pdmd.linux \
		-p $paramFile \
		-s dmd_restart \
		-i ${IFOLDRNADIR}/NMR/equi0.30.task
		-c $constraintFile
		-C $inConst

	lowestPotentialFrame=$(tail -n +3 equi_0.30.dmd_echo | sort -k5 -n | head -n 1 | awk '{print ($1+0)/10}')

	# generate pdbs, send newTopParamList to /dev/null
	#
	# ./complex_M2P.linux
	# usage: compelx.linux paramDir complexPDB newTopParamList movie outPDB [INConst [startFrame [nFrames [dFrames]]]]
	#  Dimension: x,y,z or d--Do not leave spaces in between. If ony one number is assigned, it is cubic box
	relaxed_all_atom=${rna_bname}.relaxed.AllAtom.preShake.pdb
	${IFOLDRNADIR}/complex_M2P.linux \
		${MEDUSADIR}/medusa/parameter/ \
		$all_atom \
		/dev/null \
		equi_0.30.dmd_movie \
		$relaxed_all_atom \
		$inConst \
		$lowestPotentialFrame \
		1 \
		1
    relaxed_all_atom_final=${rna_bname}.relaxed.AllAtom.pdb	
   ${IFOLDRNADIR/NMR/rna_shake.linux ${MEDUSADIR}/medusa/parameter/ ${relaxed_all_atom} > $relaxed_all_atom_final  
	echo "cluster: " $cluster
	energy=$(echo $cluster | sed 's@.*Ener:\s*\(.*\)+.*Coef.*@\1@g')
	echo "energy: " $energy

	[ "$debug" == "debug" ] && echo "Writing to output map, structure: " $relaxed_all_atom_final " energy: " $energy
	
	echo $relaxed_all_atom_final $energy >> $output_map

done
