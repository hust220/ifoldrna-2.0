#!/usr/bin/env bash

# Constrained RNA Structure Prediction Mode

EXPECTED_ARGS=12
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo  "Syntax: ./conRex.sh rnaid simTime rep0Temp rep1Temp rep2Temp rep3Temp rep4Temp rep5Temp rep6Temp rep7Temp rxInterval heatExchCo";
  exit $E_BADARGS
fi

rnaid=${1}
simTime=${2}
rep0Temp=${3}
rep1Temp=${4}
rep2Temp=${5}
rep3Temp=${6}
rep4Temp=${7}
rep5Temp=${8}
rep6Temp=${9}
rep7Temp=${10}
rxInterval=${11}
heatExchCo=${12}
boxSize=1000

STARTFILE=${rnaid}.str
RXFILE=${rnaid}.rxt
DIR=${PWD}

echo "   TXT_IN ${rnaid}.txt
  RG       1
  MIN_MASS 1.0
  TXT_OUT  ${rnaid}.txt
  TXT_DT   100000
  ECHO_OUT ${rnaid}.echo
  ECHO_DT  100
  MOV_OUT  ${rnaid}.trj
  MOV_DT   100
  THERMO_STAT_TYPE 2
  T_NEW    0.20
  HEAT_X_C ${heatExchCo}
  T_LIMIT  0.20
  MAX_TIME ${simTime}" > ${rnaid}.str

echo "    N_REPLICA     8
  RX_DT         ${rxInterval}
  REPLICA 0     ${rep0Temp}
  REPLICA 1     ${rep1Temp}
  REPLICA 2     ${rep2Temp}
  REPLICA 3     ${rep3Temp}
  REPLICA 4     ${rep4Temp}
  REPLICA 5     ${rep5Temp}
  REPLICA 6     ${rep6Temp}
  REPLICA 7     ${rep7Temp}
  RX_OUT        ${rnaid}.rxo" > ${rnaid}.rxt

# Command for Kure:
# change in patrons in final
#bsub -q week -n 8  -R "blade span[ptile=8]" -e %J.err -o %J.out -a mpichp4  mpirun.lsf  ${DIR}/dmd-rx-mc.linux -i ${STARTFILE} -r ${RXFILE}
bsub -q week -n 8 -e %J.err -o %J.out mpirun ${DIR}/dmd-rx-mc.linux -i ${STARTFILE} -r ${RXFILE}
