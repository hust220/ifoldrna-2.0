#!/bin/bash

# prerequisites
#	clustering has been performed, results stored in 'clustering.txt'
#
# post-execution
#	all-atom structures craeted,
#	structures to be returned and their energies listed in freeEnergyStructureMap.txt

#. iFoldRNAConfig.sh

export MEDUSADIR='/nas02/home/k/r/krohotin/HRP-1.0/iFoldRNA'
export IFOLDRNADIR='/nas02/home/k/r/krohotin/HRP-1.0/iFoldRNA'

# doesn't really do anything, just consistent with iFoldRNAv1
rnaid=$1
# for debugging give second argument as 'debug'
debug=$2
#debug=$1

output_map=freeEnergyStructureMap.txt
echo -e "<PDB-File-Name> <Free-Energy>" > $output_map

# for each cluster
grep 'Cluster: ' clustering.txt |
while read cluster
do
	[ "$debug" == "debug" ] && echo "Processing cluster: " $cluster
	
	# grab the lowest energy structure
        #	 rna3bead=$(echo $cluster | sed 's@.*Lowest_E_Structure: \(.*pdb\) .*@\1@g')

        # grab the centroid of the cluster
        rna3bead=$(echo $cluster | sed 's@.* Centroid: \(.*pdb\)@\1@g')

	[ "$debug" == "debug" ] && echo "Processing 3 bead model: " $rna3bead

	# use this for unique names
	rna_bname=$(basename $rna3bead .pdb)
	
	# name of the all atom model to be created
	all_atom=${rna_bname}_allatom.pdb
	
	[ "$debug" == "debug" ] && echo "Creating all atom model: " $all_atom

	# convert from 3 bead to all atom
	${IFOLDRNADIR}/MEDUSArecon_3BeadRNA.exe ${MEDUSADIR}/medusa/parameter/ $rna3bead >  $all_atom
	
	[ "$debug" == 'debug' ] && echo "Writing basepairing"

	# rewrite the basepairing in format used by complex-1.linux
	# e.g. A1 A10   -->   1 10
	inConst=${rnaid}.INconst
	cat ${rnaid}.const | sed 's/A/1\./g' | sed 's/B/2\./g' | sed 's/C/3\./g' | sed 's/D/4\./g' | sed 's/E/5\./g' | sed 's/F/6\./g' | sed 's/G/7\./g' | sed 's/H/8\./g' | sed 's/I/9\./g' | sed 's/J/10\./g' | sed 's/K/11\./g' | sed 's/L/12\./g' | sed 's/M/13\./g' | sed 's/N/14\./g' | sed 's/O/15\./g' | sed 's/P/16\./g' | sed 's/Q/17\./g' | sed 's/R/18\./g' | sed 's/S/19\./g' | sed 's/T/20\./g' | sed 's/U/21\./g' | sed 's/V/22\./g' | sed 's/W/23\./g' | sed 's/X/24\./g' | sed 's/Y/25\./g' | sed 's/Z/26\./g' | sed 's/^/BasePair /g' | tr "\t" " " > $inConst

	[ "$debug" == "debug" ] && echo "Running complex-1.linux"

	# prepare paramFile, stateFile and constraintFile for dmd
	rand_seed=7
	paramDir=${MEDUSADIR}/medusa/parameter/
	dimen=1000
	paramFile=${rna_bname}.cplxParam
	stateFile=${rna_bname}.cplxState
	constraintFile=${rna_bname}.cplxConst
	${IFOLDRNADIR}/complex-1.linux \
		-P $paramDir \
		-I $all_atom \
		-D $dimen \
		-p $paramFile \
		-s $stateFile \
		-S $rand_seed \
		-C $inConst \
		-c $constraintFile

	[ "$debug" == "debug" ] && echo "Writing thermostat taskfile"

	# prepare thermostat task file
	therm55_file=${rna_bname}.equi.T55
	temp=0.55
	restart_name=${rna_bname}.T55.dmd.restart
	echo_name=${rna_bname}.T55.dmd.echo
	movie_name=${rna_bname}.T55.dmd.mov
	${IFOLDRNADIR}/writeEquiT.sh \
		${therm55_file} \
		$temp \
		${restart_name} \
		${echo_name} \
		${movie_name}

	[ "$debug" == "debug" ] && echo "Running dmd"

	# run DMD
	#
	# ./xDMD.linux
	# usage: ./xDMD.linux -p paramFile -s stateFile -t txtFile -i taskFile -c constraintFile
	#   paramFile -- the parameter file for DMD simulation
	#   stateFile -- the state file or restart for DMD simulation
	#   txtFile   -- the txt file
	#                ONLY ONE of txtFile or (paramFile+stateFile) is REQUIRED
	#   taskFile  -- the task file
	#   constraintFile  -- the user-defined constraint file, Optional
	${IFOLDRNADIR}/xDMD.linux \
		-p $paramFile \
		-s $stateFile \
		-i $therm55_file \
		-c $constraintFile

	# generate pdbs, send newTopParamList to /dev/null
	#
	# ./complex_M2P.linux
	# usage: compelx.linux paramDir complexPDB newTopParamList movie outPDB [INConst [startFrame [nFrames [dFrames]]]]
	#  Dimension: x,y,z or d--Do not leave spaces in between. If ony one number is assigned, it is cubic box
	relaxed_all_atom=${rna_bname}.relaxed.AllAtom.pdb
	${IFOLDRNADIR}/complex_M2P.linux \
		${MEDUSADIR}/medusa/parameter/ \
		$all_atom \
		/dev/null \
		$movie_name \
		$relaxed_all_atom \
		$inConst \
		1000 \
		1 \
		1
	
	echo "cluster: " $cluster
	energy=$(echo $cluster | sed 's@.*Ener:\s*\(.*\)+.*Coef.*@\1@g')
	echo "energy: " $energy

	[ "$debug" == "debug" ] && echo "Writing to output map, structure: " $relaxed_all_atom " energy: " $energy
	
	echo $relaxed_all_atom $energy >> $output_map

done
