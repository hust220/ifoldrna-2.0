#!/bin/bash

# written this way to keep Java logic the same, could be cleaned up
if [ -f *-HRPFile ]
then
	# quick way to get the sequence length again
	len=$(echo $(cat irna.seq | wc -l) - 1 | bc)
	raw_hrp=*-HRPFile
	./conGetPDB.sh 4 irna 0 0 3bead.pdb
	./MEDUSArecon_3BeadRNA.exe medusa/parameter/ 3bead.pdb > allatom.pdb
	./HRP-1.0/bin/average-reactivity.pl $raw_hrp $len > ave.hrp
  for trj in p*trj; do j=$(basename $trj .trj).mov; mv $trj $j; done
	./HRP-1.0/bin/analyze.pl allatom.pdb irna.seq ave.hrp $len 8 clustering.txt
  ./HRP_getStructures.sh $2
elif [ -f *-NMRFile ]
then
    len=$(echo $(cat irna.seq | wc -l) - 1 | bc)
    raw_nmr=*-NMRFile
    ./conGetPDB.sh 4 irna 0 0 3bead.pdb
    ./NMR/recon_3beadRNA.linux medusa/parameters/ 3bead.pdb > allatom.pdb
    touch blankFile
   for trj in p*trj; do j=$(basename $trj .trj).mov; mv $trj $j; done
     ./HRP-1.0/bin/analyze.pl allatom.pdb irna.seq blankFile $len 8 clustering.txt
     ./NMR_getStructures.sh $2
else
	./noHRP_getStructures.sh $1 $2 $3
fi
