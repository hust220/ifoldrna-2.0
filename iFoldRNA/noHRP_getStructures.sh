#!/bin/bash

E_UNAVAILABLEPARAMS=66

# Load iFoldRNA configuration:
. iFoldRNAConfig.sh

if [ -z "$1" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <number-of-structures>"
  echo "input-type is 0 for sequence .seq and 1 for PDB file .pdb"
  exit $E_NOARGS
fi

if [ -z "$2" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <number-of-structures>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi

if [ -z "$3" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <number-of-structures>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi


inputType=${1}
rnaid=${2}
numpdbs=${3}

for i in `seq 0 7`; do  
  if [ -e "./p00${i}.${rnaid}.echo" ] 
  then true 
  else
    echo "Unable to find necessary input file: p00${i}.${rnaid}.echo" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi;
done



prevtrajidx=0
prevrxidx=100
foundpdbs=0
# Now find the N-number of minima:
lowestEnergyIndex=1;

 
${IFOLDRNADIR}/getEnergy.sh ${rnaid}

for i in `cat *ener | sort -n | head -n 200` ; do grep -n  -- "$i" *ener ; done  > ${rnaid}-LeastEnergies.txt

for k in `cat  ${rnaid}-LeastEnergies.txt` ; do 

  rxidx=${k:3:1}; 
  trajidx=`echo $k | cut -d: -f2`; 
  absdifftrajidx=`echo "sqrt(($trajidx - $prevtrajidx)*($trajidx - $prevtrajidx))" | bc `
  
  if [ "$prevrxidx" -eq "$rxidx" ] && [ "$absdifftrajidx" -le 1 ] 
  then
    echo "Rejecting conformation ${rnaid}.${rxidx}.${trajidx}.3bead.pdb"
    continue;
  fi;
  
  if [ "$foundpdbs" -eq "$numpdbs" ]  
  then
    break;
  fi;

  # Remember that the code assumes movie and echo files are stored at similar interval, so no need for rescaling movidx.
  # However, trajidx is rescaled to reflect the correct position in trajectory:
  movidx=`echo $k | cut -d: -f2`; trajidx=$[movidx*100];
  #${IFOLDRNADIR}/conGetPDB.sh ${inputType} ${rnaid} ${rxidx} ${movidx} ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.3bead.pdb
  ${IFOLDRNADIR}/conGetPDB.sh ${inputType} ${rnaid} ${rxidx} ${movidx} ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.3bead.pdb
  #sleep 1
  #echo "Generated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.3bead.pdb"

  ${IFOLDRNADIR}/MEDUSArecon_3BeadRNA.exe ${MEDUSADIR}/medusa/parameter/ ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.3bead.pdb >  ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.pdb
  #sleep 1

  ###############
  # Compute the contraints
  #${IFOLDRNADIR}/seq_movie2freq.exe p00${rxidx}.${rnaid}.trj  ${rnaid}.seq ${movidx} 1 1 |  awk '{printf("A%d A%d\n", $1, $2)}' > ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.frq
  #echo "Generated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.pdb"

  #${IFOLDRNADIR}/MEDUSARNA_pdb2txt.exe ${MEDUSADIR}/medusa/parameter/ 1000 ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.pdb ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.frq  >  ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.txt

  cat ${rnaid}.const | sed 's/A/1\./g' | sed 's/^/BasePair /g' | tr "\t" " " > ${rnaid}.INconst

  rand_seed=7
  ${IFOLDRNADIR}/complex-1.linux -P ${MEDUSADIR}/medusa/parameter/ -I ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.pdb -D 1000 -p ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.cplxParam -s ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.cplxState -S $rand_seed -C ${rnaid}.INconst -c ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.cplxConst

  ###############

  #sleep 1
  #echo "Generated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.txt"
  # Store relaxation simulation parameters:
  #echo -e "TXT_IN  ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.txt\nTXT_OUT  ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.txt\nTXT_DT 10000\nMOV_OUT ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.trj\nMOV_DT 10\nTHERMO_STAT_TYPE 2\nT_NEW 0.50\nHEAT_X_C 1.0\nT_LIMIT 0.50\nMAX_TIME 100" >  ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.start
  #sleep 1
  #echo "Generated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.start"
  #${IFOLDRNADIR}/dmd-noninteractive.exe -i ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.start

# Simulated Annealing


  ./writeEquiT.sh ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.equi.T55 0.55 ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.T55.dmd.restart ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.T55.dmd.echo ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.T55.dmd.mov

  ${IFOLDRNADIR}/xDMD.linux -p ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.cplxParam -s ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.cplxState -i ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.equi.T55 -c ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.cplxConst

  #sleep 1
  #echo "Simulated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.start"
  # The "10 1 1" ensures that we grab the final snapshot:
  #${IFOLDRNADIR}/MEDUSARNAmovie2pdb.exe ${MEDUSADIR}/parameter/ ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.3bead.pdb ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.trj ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.AllAtom.pdb 10 1 1

  ${IFOLDRNADIR}/complex_M2P.linux ${MEDUSADIR}/medusa/parameter/ ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.tmp.pdb /dev/null ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.T55.dmd.mov ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.AllAtom.pdb ${rnaid}.INconst 1000 1 1

  #sleep 1
  #echo "Generated ${rnaid}.${lowestEnergyIndex}.${rxidx}.${trajidx}.AllAtom.pdb"

  lowestEnergyIndex=$[lowestEnergyIndex+1]
  prevtrajidx=${trajidx}
  prevrxidx=${rxidx}
  foundpdbs=$[foundpdbs+1]

done

c=1;
echo -e "<PDB-File-Name> <Free-Energy>" >  freeEnergyStructureMap.txt
for i in `head -n ${numpdbs} ${rnaid}-LeastEnergies.txt`; do a=`echo $i | cut -d: -f2`; b=${i:3:1}; energy=`echo $i | cut -d: -f3`; echo  ${rnaid}.${c}.${b}.${a}00.AllAtom.pdb  $energy;    c=$[c+1] ; done  >>  freeEnergyStructureMap.txt
