#!/bin/bash

E_UNAVAILABLEPARAMS=66

# Load iFoldRNA configuration:
. iFoldRNAConfig.sh

if [ -z "$1" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <replica-index> <trajectory-snapshot-id>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi

if [ -z "$2" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <replica-index> <trajectory-snapshot-id>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi

if [ -z "$3" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <replica-index> <trajectory-snapshot-id>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi

if [ -z "$4" ]
then
  echo "Syntax: `basename $0` <input-type> <input-RNA-ID> <replica-index> <trajectory-snapshot-id>"
  echo "input-type is 1 for ndb-id (ndbid.pdb), 2 for uploaded NDB file (irna.pdb) and 3 for fasta sequence (irna.seq)"
  exit $E_NOARGS
fi



inputType=${1}
rnaid=${2}
rxidx=${3}
trajidx=${4}

for i in `seq 0 7`; do  
  if [ -e "./p00${i}.${rnaid}.trj" ] 
  then true 
  else
    echo "Unable to find necessary input file: p00${i}.${rnaid}.trj" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi
done


if [ ${inputType} == 1 ]; then
  if [ -e "./${rnaid}.pdb" ] ; then 
  true 
  else
    echo "Unable to find necessary input file: ${rnaid}.pdb" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi
  if [ -z "$5" ] 
    then 
    outputFileName="p00${rxidx}.${rnaid}.${trajidx}.pdb"
  else 
    outputFileName=${5}; 
  fi
  #${IFOLDRNADIR}/movie2pdb.exe p00${rxidx}.${rnaid}.trj ${rnaid}.pdb ${outputFileName} ${trajidx} 1 1 
  ${IFOLDRNADIR}/nseq_movie2pdb.linux p00${rxidx}.${rnaid}.trj ${rnaid}.seq ${outputFileName} ${trajidx} 1 1
fi

if [ ${inputType} == 2 ]; then
  if [ -e "./${rnaid}.pdb" ] ; then 
  true 
  else
    echo "Unable to find necessary input file: ${rnaid}.pdb" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi
  if [ -z "$5" ] 
    then 
    outputFileName="p00${rxidx}.${rnaid}.${trajidx}.pdb"
  else 
    outputFileName=${5}; 
  fi
  #${IFOLDRNADIR}/movie2pdb.exe p00${rxidx}.${rnaid}.trj ${rnaid}.pdb ${outputFileName} ${trajidx} 1 1 
  ${IFOLDRNADIR}/nseq_movie2pdb.linux p00${rxidx}.${rnaid}.trj ${rnaid}.seq ${outputFileName} ${trajidx} 1 1
fi

#for constrained runs, should only be inputType 4
if [ ${inputType} == 4 ] ; then
  if [ -e "./${rnaid}.seq" ] ; then
    true
  else
    echo "Unable to find necessary input file: ${rnaid}.seq" ;
    exit $E_UNAVAILABLEPARAMS ; 
  fi
  if [ -z "$5" ]
    then 
    outputFileName="p00${rxidx}.${rnaid}.${trajidx}.pdb"
  else 
    outputFileName=${5}; 
  fi
  ${IFOLDRNADIR}/HRP-1.0/bin/nseq_movie2pdb.linux p00${rxidx}.${rnaid}.trj ${rnaid}.seq ${outputFileName} ${trajidx} 1 1
fi
