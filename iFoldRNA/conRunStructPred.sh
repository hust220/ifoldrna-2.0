#!/usr/bin/env bash

# Constrained RNA Structure Prediction Mode

# inputType specifies whether RNA ID/PDB file/RNA Sequence
inputType=${1};

# Load iFoldRNA configuration:
. iFoldRNAConfig.sh

EXPECTED_ARGS=16
E_BADARGS=65

echo $#

if [ $# -ne $EXPECTED_ARGS ]
then
  echo  "Syntax: ./conRunStructPred.sh inputType rnaid/rnaFileName/rnaSeq simTime rep0Temp rep1Temp rep2Temp rep3Temp rep4Temp rep5Temp rep6Temp rep7Temp rxInterval heatExchCo basePairs LR-constraints";
  echo  "Use inputType=1 for specifying Nucleic Acid DataBank ID as input" ;
  echo  "Use inputType=2 for specifying an NDB File as input";
  echo  "Use inputType=3 for specifying a sequence as input";
  exit $E_NOARGS
fi


if [ -z "$1" ]
then
  echo  "Syntax: ./conRunStructPred.sh inputType rnaid/rnaFileName/rnaSeq simTime rep0Temp rep1Temp rep2Temp rep3Temp rep4Temp rep5Temp rep6Temp rep7Temp rxInterval heatExchCo basePairs LR-constraints";
  echo  "Use inputType=1 for specifying Nucleic Acid DataBank ID as input" ;
  echo  "Use inputType=2 for specifying an NDB File as input";
  echo  "Use inputType=3 for specifying a sequence as input";
  exit $E_NOARGS
fi

# Older fix for broken DMD-MPI, no longer needed.
#tmpTime=${3} ; if [ $[tmpTime%10] -ne 1 ] ; then simTime=$[tmpTime+1] ; else simTime=${tmpTime}; fi
simTime=${3} ;
rep0Temp=${4} ;
rep1Temp=${5} ;
rep2Temp=${6} ;
rep3Temp=${7} ;
rep4Temp=${8} ;
rep5Temp=${9} ;
rep6Temp=${10} ;
rep7Temp=${11};
rxInterval=${12};
heatExchCo=${13};
boxSize=1000 ;

# KH: new stuff
basePairing=${15};
constraints=${16};
# end new
boxSize=1000;

# leave switch to make compatible with runStructPred.sh
# scripts might be reconciled and merged in the future

case "$inputType" in
  '4') # rna sequence is the input, constrained run
    rnaid="irna";
    echo ${2} | grep -v '>' | tr 'a-z' 'A-Z' |  sed 's/T/U/g' |  sed 's/[BD-FH-TV-Z]//g' | sed 's/[ACGU]/&\
/g' | grep '[ACGU]' >  ${rnaid}.seq
    echo "END" >> ${rnaid}.seq

    echo "${15}" | sed 's/\([0-9]\+\)/A\1/g' | sed 's/\([0-9]\+\)\s\+A/\1\tA/g' | sed 's///g' >  ${rnaid}.const

    # ${IFOLDRNADIR}/HRP-1.0/bin/nseq_cons2txt.linux irna.seq ${rnaid}.const ${boxSize} > ${rnaid}.txt
  
    STARTFILE=${rnaid}.str
    RXFILE=${rnaid}.rxt
    DIR=${PWD}
  
    echo "   TXT_IN ${rnaid}.txt
      RG       1
      MIN_MASS 1.0
      TXT_OUT  ${rnaid}.txt
      TXT_DT   100000
      ECHO_OUT ${rnaid}.echo
      ECHO_DT  100
      MOV_OUT  ${rnaid}.trj
      MOV_DT   100
      THERMO_STAT_TYPE 2
      T_NEW    0.20
      HEAT_X_C ${heatExchCo}
      T_LIMIT  0.20
      MAX_TIME ${simTime}" > ${rnaid}.str

    echo "    N_REPLICA     8
      RX_DT         ${rxInterval}
      REPLICA 0     ${rep0Temp}
      REPLICA 1     ${rep1Temp}
      REPLICA 2     ${rep2Temp}
      REPLICA 3     ${rep3Temp}
      REPLICA 4     ${rep4Temp}
      REPLICA 5     ${rep5Temp}
      REPLICA 6     ${rep6Temp}
      REPLICA 7     ${rep7Temp}
      RX_OUT        ${rnaid}.rxo" > ${rnaid}.rxt

    # Command for Kure:
    # change in patrons in final
#    bsub -q week -n 8  -R "blade span[ptile=8]" -e %J.err -o %J.out -a mpichp4  mpirun.lsf  ${DIR}/dmd-rx-mc.linux -i ${STARTFILE} -r ${RXFILE}
     bsub -q week -n 8 -e %J.err -o %J.out mpirun ${DIR}/dmd-rx-mc.linux -i ${STARTFILE} -r ${RXFILE}

  ;;
  *)
    echo  "Syntax:./runStructPred.sh inputType rnaid/rnaFileName/rnaSeq simTime rep0Temp rep1Temp rep2Temp rep3Temp rep4Temp rep5Temp rep6Temp rep7Temp rxInterval heatExchCo";
    echo  "Use inputType=1 for specifying Nucleic Acid DataBank ID as input" ;
    echo  "Use inputType=2 for specifying an NDB File as input";
    echo  "Use inputType=3 for specifying a sequence as input";
esac


