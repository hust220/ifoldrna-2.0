#!/usr/bin/env bash

# Create .seq and .const files from strings

EXPECTED_ARGS=3
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo  "Syntax: ./conSeqConst.sh rnaSeq basePairing rnaid";
  exit $E_BADARGS
fi

rnaSeq=${1}
basePairing=${2}
rnaid=${3}

echo $rnaSeq | grep -v '>' | tr -d ' ' | tr 'a-z' 'A-Z' |  sed 's/T/U/g' |  sed 's/[BD-FH-TV-Z]//g' | sed 's/[ACGU]/&\
/g' | grep '[ACGU]' >  ${rnaid}.seq
echo "END" >> ${rnaid}.seq

echo "${basePairing}" > temp
dos2unix temp 
cat temp | sed '/^$/d' | awk '{if($1<$2){print $1 " " $2}else{print $2 " " $1}}' | sort | uniq | sed 's/\([0-9]\+\)/A\1/g' | sed 's/\([0-9]\+\)\s\+A/\1\tA/g' > ${rnaid}.const
rm temp 
